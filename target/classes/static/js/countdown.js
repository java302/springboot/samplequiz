function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function() {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

 

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

 

        display.textContent = minutes + ":" + seconds;

 

        if (--timer < 0) {
            timer = duration;
        }
        if (timer == 0) {
            document.getElementById('Submit').click();
            clearSession()
        }
        console.log(parseInt(seconds))
        window.sessionStorage.setItem("seconds", seconds)
        window.sessionStorage.setItem("minutes", minutes)
    }, 1000);
}

 

window.onload = function() {
    sec = parseInt(window.sessionStorage.getItem("seconds"))
    min = parseInt(window.sessionStorage.getItem("minutes"))

 

    if (parseInt(min * sec)) {
        var fiveMinutes = (parseInt(min * 60) + sec);
    } else {
        var fiveMinutes = 60 * 5;
    }
    // var fiveMinutes = 60 * 5;
    display = document.querySelector('#time');
    startTimer(fiveMinutes, display);
};
function clearSession() {
    sessionStorage.clear()
}