function validate()
{
	var email = document.getElementById("email").value;
	var pass = document.getElementById("password").value;

	if ((email == null || email == "") && (pass == "" || pass == null))
	{
		alert("Email ID & Password Cannot Be Empty. Please Enter Email ID & Password.");
		return false;
	}

	else if (email == null || email == "")
	{
		alert("Email ID Cannot Be Empty. Please Enter Email ID.");
		return false;
	}

	else if (pass == "" || pass == null)
	{
		alert("Password Cannot Be Empty. Please Enter Password.")
		return false;
	}

	else
	{
		return true;
	}
}