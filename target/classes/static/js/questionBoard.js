//This logic has been modified
function enable_disable(element) {

	if (element.id != null && element.id != '') {
		//questionButton
		var questionButton = document.getElementById('questionButton').value;
		localStorage.setItem("questionButton", questionButton);
	}
	localStorage.setItem("selectedOption", element.value);
	localStorage.setItem("selectedOptionCode", element.id);
	var correct_ans = document.getElementById('correct_ans').value;
	$("#formDisable :input").prop("disabled", true);
	if (correct_ans == element.id) {
		element.style.backgroundColor = "green";
		$("#" + element.id).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
	} else {
		element.style.backgroundColor = "red";
		document.getElementById(correct_ans).style.backgroundColor = "green";
		$("#" + correct_ans).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
	}
}




//Logic modified
function optionValidation(element) {
	var correctAnswer = document.getElementById('correctAnswer').value;
	localStorage.setItem("correctAnswer", correctAnswer);
	sendSelectedOption();
	
	if ((element.id != "optionA") && (element.id != "optionB") && (element.id != "optionC") && (element.id != "optionD")) {
		return true;
	}
	return false;
}

function sendSelectedOption() {
	var selectedCodeValue = localStorage.getItem("selectedOptionCode");
	var selectedValue = localStorage.getItem("selectedOption");
	var questionCode = localStorage.getItem("questionButton");
    var correctAnswer = localStorage.getItem("correctAnswer");
	
	localStorage.removeItem("selectedOption");
	localStorage.removeItem("selectedOptionCode");
	localStorage.removeItem("correctAnswer");	
	$(document).ready(function() {
		$.ajax({
			url: 'http://localhost:8080/quiz/getOptionValue',
			type: 'GET',
			contentType: "application/json",
			dataType: "json",
			data: {
				selectedValue: selectedValue,
				questionCode: questionCode,
				selectedCode: selectedCodeValue,
				correctAnswer: correctAnswer
			},
			success: function(data) {
				console.log(data);
			}
		});
	});
}