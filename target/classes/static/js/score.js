//For Score page only
window.onload = function(evnt) {
    setTimeout("preventBack()", 0);
    window.onunload = function() { null };

    var myEle = document.getElementById("result_score");
    if (myEle) {
        myEle.innerHTML = myEle.innerHTML + result() + " / 10";
    }
    //logic modified
    var facts = document.getElementsByClassName("facts")[0];
    if (facts) {
        showHideFacts(facts, false);
    }
}

//Go back disabled
function preventBack() { window.history.forward(); }

//This logic has been modified
function enable_disable(element) {
    var correct_ans = document.getElementById('correct_ans').value;
    $("#formDisable :input").prop("disabled", true);
    if (correct_ans == element.id) {
        element.style.backgroundColor = "green";
        document.getElementById('chkbtn').value = "1";
        var facts = document.getElementsByClassName("facts")[0];
        showHideFacts(facts, true);
    } else {
        element.style.backgroundColor = "red";
        document.getElementById(correct_ans).style.backgroundColor = "green";
        document.getElementById('chkbtn').value = "0";
        var facts = document.getElementsByClassName("facts")[0];
        showHideFacts(facts, true);
    }
}

//Newly added
function showHideFacts(element, option) {
    if (option) {
        element.style.display = "block";
        element.style.color = "blue";
    } else {
        element.style.display = "none";
    }
}

//Newly added function to count and pass score
function getScore() {
    var queryString = decodeURIComponent(window.location.search);
    queryString = queryString.substring(1);
    if (queryString != "") {
        var queries = queryString.split("=");
        return parseInt(queries[1]) + parseInt(document.getElementById('chkbtn').value);
    } else {
        return parseInt(document.getElementById('chkbtn').value);
    }
}

//Only callable for score page
function result() {
    var queryString = decodeURIComponent(window.location.search);
    queryString = queryString.substring(1);
    if (queryString != "") {
        var queries = queryString.split("=");
        return parseInt(queries[1]);
    } else {
        return 0;
    }
}

//Logic modified
function optionValidation(element) {
    if ((element.id != "optionA") && (element.id != "optionB") && (element.id != "optionC") && (element.id != "optionD")) {

        if (document.getElementById('chkbtn').value != "") {
            return true;
        } else {
            alert("Please choose an option.");
        }
    }
    return false;
}