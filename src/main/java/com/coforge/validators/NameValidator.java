package com.coforge.validators;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.coforge.daos.UserDao;
import com.coforge.entities.User;

@Component
public class NameValidator implements Validator {
	
	private static final Logger LOG = LoggerFactory.getLogger(NameValidator.class);

	@Autowired
	private UserDao userDao;

	public boolean supports(Class<?> cl) {

		return cl.equals(User.class);
	}

	public void validate(Object target, Errors errors) {

		User user = (User) target;
		try {
			List<User> list = userDao.findByName(user.getName());
			if (!list.isEmpty()) {
				errors.rejectValue("name", "name", "User ID is already registered.");
			}
		} catch (Exception e) {
			LOG.error("Unexpected Error : "+e);
		}
	}
}