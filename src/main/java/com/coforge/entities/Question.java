package com.coforge.entities;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class Question {

	private String categoryCode;
	private String categoryName;
	private String questionName;
	private String questionCode;
	private String option1;
	private String option2;
	private String option3;
	private String option4;
	private String optionCode;
	private String correctOption;
	private String answerCode;
	private List<String> options;
	private String categoryId;
	private String question;
	private List<Option> optionsList;
	private String ansCode;
	private String ans;
	private String option;
	private String result;
	private int pageId;
	private int nextPageId;
	private String selectedOption;
	private String selectedOptionCode;

	public Question() {
		super();
	}

	public Question(String categoryCode, String categoryName, String questionName, String questionCode, String option1,
			String option2, String option3, String option4, String optionCode, String correctOption, String answerCode,
			List<String> options, String categoryId, String question, List<Option> optionsList, String ansCode,
			String ans, String option, String result, int pageId, int nextPageId, String selectedOption,
			String selectedOptionCode) {
		super();
		this.categoryCode = categoryCode;
		this.categoryName = categoryName;
		this.questionName = questionName;
		this.questionCode = questionCode;
		this.option1 = option1;
		this.option2 = option2;
		this.option3 = option3;
		this.option4 = option4;
		this.optionCode = optionCode;
		this.correctOption = correctOption;
		this.answerCode = answerCode;
		this.options = options;
		this.categoryId = categoryId;
		this.question = question;
		this.optionsList = optionsList;
		this.ansCode = ansCode;
		this.ans = ans;
		this.option = option;
		this.result = result;
		this.pageId = pageId;
		this.nextPageId = nextPageId;
		this.selectedOption = selectedOption;
		this.selectedOptionCode = selectedOptionCode;
	}

	public Question(String questionCode, String questionName) {
		super();
		this.questionCode = questionCode;
		this.questionName = questionName;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getQuestionName() {
		return questionName;
	}

	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public String getQuestionCode() {
		return questionCode;
	}

	public void setQuestionCode(String questionCode) {
		this.questionCode = questionCode;
	}

	public String getOption1() {
		return option1;
	}

	public void setOption1(String option1) {
		this.option1 = option1;
	}

	public String getOption2() {
		return option2;
	}

	public void setOption2(String option2) {
		this.option2 = option2;
	}

	public String getOption3() {
		return option3;
	}

	public void setOption3(String option3) {
		this.option3 = option3;
	}

	public String getOption4() {
		return option4;
	}

	public void setOption4(String option4) {
		this.option4 = option4;
	}

	public String getOptionCode() {
		return optionCode;
	}

	public void setOptionCode(String optionCode) {
		this.optionCode = optionCode;
	}

	public String getCorrectOption() {
		return correctOption;
	}

	public void setCorrectOption(String correctOption) {
		this.correctOption = correctOption;
	}

	public String getAnswerCode() {
		return answerCode;
	}

	public void setAnswerCode(String answerCode) {
		this.answerCode = answerCode;
	}

	public List<String> getOptions() {
		return options;
	}

	public void setOptions(List<String> options) {
		this.options = options;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public List<Option> getOptionsList() {
		return optionsList;
	}

	public void setOptionsList(List<Option> optionsList) {
		this.optionsList = optionsList;
	}

	public String getAnsCode() {
		return ansCode;
	}

	public void setAnsCode(String ansCode) {
		this.ansCode = ansCode;
	}

	public String getAns() {
		return ans;
	}

	public void setAns(String ans) {
		this.ans = ans;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public int getPageId() {
		return pageId;
	}

	public void setPageId(int pageId) {
		this.pageId = pageId;
	}

	public int getNextPageId() {
		return nextPageId;
	}

	public void setNextPageId(int nextPageId) {
		this.nextPageId = nextPageId;
	}

	public String getSelectedOption() {
		return selectedOption;
	}

	public void setSelectedOption(String selectedOption) {
		this.selectedOption = selectedOption;
	}

	public String getSelectedOptionCode() {
		return selectedOptionCode;
	}

	public void setSelectedOptionCode(String selectedOptionCode) {
		this.selectedOptionCode = selectedOptionCode;
	}

}
