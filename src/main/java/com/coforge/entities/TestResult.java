package com.coforge.entities;

public class TestResult {

	private int id;
	private String name;
	private String mailId;
	private String category;
	private String marks;
	private String date;

	public TestResult() {
		super();
	}

	public TestResult(int id, String name, String mailId, String category, String marks, String date) {
		super();
		this.id = id;
		this.name = name;
		this.mailId = mailId;
		this.category = category;
		this.marks = marks;
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getMarks() {
		return marks;
	}

	public void setMarks(String marks) {
		this.marks = marks;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
