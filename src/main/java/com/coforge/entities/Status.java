package com.coforge.entities;

import org.springframework.stereotype.Component;

@Component
public class Status {

	private boolean status;

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
