package com.coforge.entities;

public class Option {

	private String optionCode;
	private String option;

	public Option() {
		super();
	}

	public Option(String optionCode, String option) {
		super();
		this.optionCode = optionCode;
		this.option = option;
	}

	public String getOptionCode() {
		return optionCode;
	}

	public void setOptionCode(String optionCode) {
		this.optionCode = optionCode;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

}
