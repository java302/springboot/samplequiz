package com.coforge.entities;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Component;

@Component
public class User {

	private int id;

	@Pattern(regexp = "^[A-Z,a-z,0-9]{2,15}$", message = "User ID can contain alphanumeric values of length 2 to 15.")
	private String name;

	@Email(message = "Email ID should be in proper format.")
	@NotBlank(message = "Email ID is required.")
	private String mailId;

	@Length(min = 5, message = "Password must have at least 5 characters.")
	private String password;

	private String role;
	private String msg;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}