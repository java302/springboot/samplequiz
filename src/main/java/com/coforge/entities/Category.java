package com.coforge.entities;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class Category {

	private String categoryCode;
	private String categoryName;
	private List<String> categoryList;

	public Category() {
	}

	public Category(String categoryCode, String categoryName, List<String> categoryList) {
		super();
		this.categoryCode = categoryCode;
		this.categoryName = categoryName;
		this.categoryList = categoryList;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<String> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<String> categoryList) {
		this.categoryList = categoryList;
	}

}
