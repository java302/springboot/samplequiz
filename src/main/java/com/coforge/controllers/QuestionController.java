package com.coforge.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.coforge.entities.Question;
import com.coforge.entities.QuizChallengeDetails;
import com.coforge.entities.TestResult;
import com.coforge.entities.User;
import com.coforge.service.QuestionService;
import com.coforge.service.UserService;
import com.coforge.utils.SendEmail;

@Controller
@RequestMapping("/quiz")
public class QuestionController {

	private static final Logger LOG = LoggerFactory.getLogger(QuestionController.class);

	@Autowired
	private HttpSession httpSession;
	@Autowired
	private QuestionService questionService;
	@Autowired
	private UserService userService;

	@Value("${app.usermovement.maxlimit:2}")
	public String maxLimit;

	@Value("${app.questioncount.maxlimit:10}")
	public int quesMaxLimit;

	@Value("${app.questioncount.minlimit:10}")
	public int quesMinLimit;

	int score = 0;

	@Value("${app.email.from:}")
	public String fromEmail;

	@Value("${app.email.from.password:}")
	public String emailPassword;

	@RequestMapping("/")
	public ModelAndView home(HttpServletRequest request) {

		ModelAndView modelAndView = new ModelAndView();
		if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {
			modelAndView.setViewName("home");
		} else {
			modelAndView.setViewName("session-expried-or-null");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/getQuestions", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView login(@ModelAttribute User user, HttpServletRequest request, @RequestParam String categoryCode,
			@RequestParam String page, @RequestParam String challengeId) throws Exception {
		ModelAndView modelAndView = new ModelAndView();

		int iChallengeId = 0;
		boolean isAuthorized = false;
		if (null != challengeId && !challengeId.trim().equals("")) {
			iChallengeId = Integer.parseInt(challengeId);
		}
		modelAndView.addObject("challengeId", iChallengeId);

		if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {

			String categoryId = request.getParameter("categoryCode");
			int pageId = Integer.parseInt(request.getParameter("page"));

			if (null == httpSession.getAttribute("moveOutFromPage")) {
				request.getSession(false).setAttribute("moveOutFromPage", 0);
			}

			List<Question> questions = new ArrayList<Question>();
			if (pageId == 0) {
				questions = (List<Question>) httpSession.getAttribute("questionList");
				if (questions == null || questions.size() == 0) {
					score = 0;
					if (iChallengeId > 0) {
						user = (User) request.getSession(false).getAttribute("user");
						LOG.info("Authorization check for Challenge Quiz:" + user.getMailId());
						isAuthorized = questionService.authorizationCheckBychallengeId(iChallengeId, user.getMailId());
						if (isAuthorized) {
							questions = questionService.findQuestionsDetailsForChallengeID(categoryId, challengeId);
						}
					} else {
						questions = questionService.findRandomQuesByCategory(categoryId, quesMaxLimit);
					}
				}
			} else {
				questions = (List<Question>) httpSession.getAttribute("questionList");

			}

			if (null != questions) {
				for (Question question : questions) {
					question.setCategoryId(categoryId);
					question.setPageId(pageId);
					pageId = pageId + 1;
					int nextPageId = pageId;
					question.setNextPageId(nextPageId);
				}
			}
			String categoryName = questionService.getCategoryName(categoryId);
			if (questions != null && !questions.isEmpty()) {
				if (questions.size() < quesMinLimit) {
					modelAndView.addObject("categoryName", categoryName);
					modelAndView.addObject("quesMinLimit", quesMinLimit);
					modelAndView.setViewName("errorQuestions");
				} else {
					httpSession.setAttribute("questionList", questions);
					modelAndView.addObject("questionList", questions);
					modelAndView.setViewName("questionBoard");
				}

			} else {
				if (!isAuthorized) {
					modelAndView.setViewName("unauthorizeQuizAccess");
				} else {
					modelAndView.addObject("categoryName", categoryName);
					modelAndView.setViewName("noQuestions");
				}
			}

		} else {
			modelAndView.addObject("categoryCode", categoryCode);
			modelAndView.addObject("page", page);
			modelAndView.setViewName("session-expried-or-null");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/getOptionValue", method = RequestMethod.GET)
	public @ResponseBody String selectedOption(ModelMap map, @RequestParam("selectedValue") String selectedValue,
			@RequestParam("questionCode") String questionCode, @RequestParam("selectedCode") String selectedCode,
			@RequestParam("correctAnswer") String correctAnswer, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		if (correctAnswer.equals(selectedValue)) {
			score = score + 1;
		} else {
			score = score + 0;
		}

		LOG.info("Score : " + score);

		List<Question> questionList = (List<Question>) httpSession.getAttribute("questionList");
		String selectedOpValue = request.getParameter("selectedValue");
		String questionValue = request.getParameter("questionCode");
		String selectedOpCode = request.getParameter("selectedCode");
		for (Question ques : questionList) {
			if (ques.getQuestionCode().equals(questionValue)) {
				if (selectedOpValue != null && selectedOpValue != "") {
					ques.setSelectedOption(selectedOpValue);
					ques.setSelectedOptionCode(selectedOpCode);
				}
			}
		}
		httpSession.setAttribute("questionList", questionList);

		return selectedValue;
	}

	@RequestMapping(value = "/submitQuiz", method = RequestMethod.GET)
	public ModelAndView submitQuiz(@ModelAttribute User user, HttpServletRequest request,
			@ModelAttribute TestResult testResult) throws Exception {
		user = (User) httpSession.getAttribute("user");
		ModelAndView modelAndView = new ModelAndView();
		if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {
			Question questionObj = new Question();
			String categoryCode = request.getParameter("categoryCode");
			List<Question> questions = (List<Question>) httpSession.getAttribute("questionList");
			String totalScore = score + "/" + questions.size();
			String categoryName = questionService.getCategoryName(categoryCode);
			questionObj.setCategoryName(categoryName);
			questionObj.setResult(totalScore);
			testResult.setName(user.getName());
			testResult.setMailId(user.getMailId());
			testResult.setCategory(categoryName);
			testResult.setMarks(totalScore);
			questionService.saveResult(testResult);
			modelAndView.addObject("Result", questionObj);
			modelAndView.addObject("user", user);
			modelAndView.setViewName("score");
			if (httpSession.getAttribute("questionList") != null) {
				httpSession.removeAttribute("questionList");
			}
			if (httpSession.getAttribute("selectedValue") != null) {
				httpSession.removeAttribute("selectedValue");
			}
		} else {
			modelAndView.setViewName("session-expried-or-null");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/trackUserMovment", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView trackUserIfMovingOutFromPage(@ModelAttribute User user, HttpServletRequest request)
			throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {

			LOG.info("trackUserMovment " + request.getSession(false).getAttribute("moveOutFromPage"));
			int userMovementCount = (int) httpSession.getAttribute("moveOutFromPage");
			userMovementCount++;
			request.getSession(false).setAttribute("moveOutFromPage", userMovementCount);
			modelAndView.addObject("userMovementCnt", userMovementCount);
			modelAndView.setViewName("trackUserMovement");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/submitQuizCategory", method = RequestMethod.GET)
	public ModelAndView challengeQuiz(HttpServletRequest request) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {
			List<Map<String, Object>> categoryList = userService.getAllCategories();
			request.setAttribute("categoryList", categoryList);
			modelAndView.addObject("categoryList", categoryList);
			modelAndView.setViewName("challengeCategory");
		} else {
			modelAndView.setViewName("session-expried-or-null");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/challengeQuiz", method = RequestMethod.GET)
	public ModelAndView challengeQuizQuestion(HttpServletRequest request) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {
			String selectdCategory = request.getParameter("select_category");
			List<Question> questionList = questionService.getQuesByCategory(selectdCategory);

			List<Map<String, Object>> categoryList = userService.getAllCategories();
			request.setAttribute("categoryList", categoryList);
			modelAndView.addObject("selectdCategory", selectdCategory);
			modelAndView.addObject("questionList", questionList);
			modelAndView.setViewName("quiz");
		} else {
			modelAndView.setViewName("session-expried-or-null");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/review", method = RequestMethod.POST)
	public ModelAndView challengeQuizReview(HttpServletRequest request) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {
			List<Map<String, Object>> categoryList = userService.getAllCategories();
			String[] qList = request.getParameterValues("qList");
			List<Question> questionList = questionService.getQuestionsFromQCode(qList);
			request.setAttribute("categoryList", categoryList);
			modelAndView.addObject("categoryList", categoryList);
			modelAndView.addObject("questionList", questionList);
			modelAndView.setViewName("review");
		} else {
			modelAndView.setViewName("session-expried-or-null");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/challengeSend", method = RequestMethod.POST)
	public ModelAndView challengeQuizSend(HttpServletRequest request, @RequestParam String questionsList)
			throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {
			String category = request.getParameter("category");
			LOG.info("sQuestionsList: " + questionsList);
			LOG.info("Category: " + category);
			String challengesIds[] = questionsList.split(",");
			List<QuizChallengeDetails> quizChallengeDetList = new ArrayList<QuizChallengeDetails>();
			for (int i = 0; i < challengesIds.length; i++) {
				QuizChallengeDetails quizChallengeDetails = new QuizChallengeDetails();
				// quizChallengeDetails.setChallengeId(1);
				quizChallengeDetails.setQuizId(challengesIds[i]);
				quizChallengeDetails.setQuestionCode(challengesIds[i]);
				quizChallengeDetList.add(quizChallengeDetails);
			}

			int challengeId = questionService.saveChallenge(quizChallengeDetList, category);
			modelAndView.addObject("challenge_id", challengeId);
			modelAndView.setViewName("quizChallenge");
		} else {
			modelAndView.setViewName("session-expried-or-null");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/challengeSent", method = RequestMethod.POST)
	public ModelAndView challengeQuizSent(HttpServletRequest request, @RequestParam String emailIds) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {
			String challengeId = request.getParameter("challenge_id");
			String category = request.getParameter("category");

			LOG.info("challengeSent: categoryCode - " + request.getParameter("category"));
			LOG.info("challengeSent: challenge_id - " + request.getParameter("challenge_id"));

			List<QuizChallengeDetails> quizChallengeContestantDet = questionService
					.saveChallengeContestantDetails(Integer.parseInt(challengeId), emailIds.toLowerCase());

			SendEmail sendEmail = new SendEmail();
			List<QuizChallengeDetails> updQuizChallengeContestantDet = sendEmail.emailSend(quizChallengeContestantDet,
					category, fromEmail, emailPassword);

			modelAndView.addObject("updQuizChallengeContestantDet", updQuizChallengeContestantDet);
			modelAndView.setViewName("challengeSent");
		} else {
			modelAndView.setViewName("session-expried-or-null");
		}
		return modelAndView;
	}
}
