package com.coforge.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.coforge.entities.Category;
import com.coforge.entities.Question;
import com.coforge.entities.Status;
import com.coforge.service.IExcelDataService;
import com.coforge.service.IFileUploaderService;
import com.coforge.service.QuestionAddService;
import com.coforge.service.UserService;

@Controller
@RequestMapping("/question")
public class QuestionAddController {

	private static final Logger LOG = LoggerFactory.getLogger(QuestionAddController.class);

	@Autowired
	private QuestionAddService questionAddService;

	@Autowired
	private UserService userService;

	@Autowired
	IFileUploaderService ifileUploaderService;

	@Autowired
	IExcelDataService iexcelDataService;

	@RequestMapping("/")
	public ModelAndView home(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {
			List<Map<String, Object>> categoryList = questionAddService.getAllCategories();
			Question question = new Question();
			request.setAttribute("question", question);
			request.setAttribute("categoryList", categoryList);
			modelAndView.setViewName("addQuestions");
		} else {
			modelAndView.setViewName("session-expried-or-null");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/questions", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Status saveQuestions(@RequestBody Question question) {
		List<String> option = new ArrayList<String>();
		question.setQuestionCode(questionAddService.getQuestionCount());
		question.setOptionCode(questionAddService.getOptionCount());
		option.add(question.getOption1());
		option.add(question.getOption2());
		option.add(question.getOption3());
		option.add(question.getOption4());
		question.setOptions(option);
		boolean state = false;
		try {
			boolean checkQuestion = questionAddService.checkQuestion(question.getQuestionName());
			if (checkQuestion == true) {
				LOG.error(question.getQuestionName() + " alreay exist in database");
			} else {
				state = questionAddService.saveQuestion(question);
				LOG.info("Data saved in question table");
			}
		} catch (Exception e) {
			LOG.error("Unexpected error", e);
		}
		if (state == true) {
			state = questionAddService.saveOptions(question);
		}
		if (state == true) {
			question.setAnswerCode(questionAddService.getAnswerCount());
			state = questionAddService.saveAnswer(question);
		}
		Status status = new Status();
		status.setStatus(state);
		return status;
	}

	@RequestMapping("/category")
	public ModelAndView categoryHome(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {
			modelAndView.setViewName("category");
		} else {
			modelAndView.setViewName("session-expried-or-null");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/categories", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Status saveCategory(@RequestBody Category category) {

		boolean state = questionAddService.saveCategory(category);
		Status status = new Status();
		status.setStatus(state);
		return status;
	}

	@GetMapping("/ques")
	public ModelAndView index(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {
			modelAndView.setViewName("uploadPage");
		} else {
			modelAndView.setViewName("session-expried-or-null");
		}
		return modelAndView;
	}

	@PostMapping("/uploadFile")
	public ModelAndView uploadFile(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes,
			HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {
			ifileUploaderService.uploadFile(file);

			redirectAttributes.addFlashAttribute("message",
					"You have successfully uploaded '" + file.getOriginalFilename() + "' !");

			redirectAttributes.addFlashAttribute("fname", file.getOriginalFilename());
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				LOG.error("Unexpected Error : " + e);
			}
			modelAndView.setViewName("redirect:/question/ques");
		} else {
			modelAndView.setViewName("session-expried-or-null");
		}
		return modelAndView;
	}

	@GetMapping("/saveData")
	public String saveExcelData(Model model, @RequestParam("fname") String file, HttpServletRequest request) {
		boolean status = false;
		List<Question> excelDataAsList = iexcelDataService.getExcelDataAsList(file);
		if (excelDataAsList.size() != 0) {
			status = questionAddService.saveQuestionFromExcel(excelDataAsList);
		}
		List<Map<String, Object>> categoryList = userService.getAllCategories();
		request.setAttribute("categoryList", categoryList);
		if (status == true) {
			return "databaseSuccess";
		} else {
			return "databaseError";
		}
	}

}
