package com.coforge.controllers;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.coforge.entities.TestResult;
import com.coforge.entities.User;
import com.coforge.service.QuestionService;
import com.coforge.service.UserService;
import com.coforge.validators.MailValidator;
import com.coforge.validators.NameValidator;

@Controller
@RequestMapping("/")
public class UserController {

	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private HttpSession httpSession;

	@Autowired
	private MailValidator mailValidator;

	@Autowired
	private NameValidator nameValidator;

	@Autowired
	private QuestionService questionService;

	// method to process the request for home page
	@RequestMapping("/")
	public String home() {
		return "index";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ModelAndView register(@Valid @ModelAttribute User user, BindingResult br) throws Exception {
		// custom validation is performed
		mailValidator.validate(user, br);
		nameValidator.validate(user, br);
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("user", user);
		if (br.hasErrors()) {
			modelAndView.setViewName("registerForm");
		} else {
			userService.save(user);
			modelAndView.setViewName("registered");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView login(@ModelAttribute User user, @RequestParam String name, @RequestParam String password,
			HttpServletRequest request, @RequestParam String categoryCode, @RequestParam String page,
			@RequestParam String challengeId) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		boolean isAuthorized = false;
		int iChallengeId = 0;
		user = userService.findByName(name);

		// encoding pass enterd by user to varify login
		Base64.Encoder encoder = Base64.getEncoder();
		String normalString = password;
		String encodedString = encoder.encodeToString(normalString.getBytes(StandardCharsets.UTF_8));
		password = encodedString;

		if (user == null) {
			modelAndView.setViewName("relogin");
			return modelAndView;
		}

		else {
			if (user.getPassword().equals(password)) {
				httpSession = request.getSession();
				httpSession.setMaxInactiveInterval(6000);
				httpSession.setAttribute("user", user);
				modelAndView.addObject("user", user);
				List<Map<String, Object>> categoryList = userService.getAllCategories();
				request.setAttribute("categoryList", categoryList);
				request.setAttribute("categoryCode", categoryCode);
				request.setAttribute("page", page);
				request.setAttribute("challengeId", challengeId);

				if (null != challengeId && !"".equals(challengeId)) {
					iChallengeId = Integer.parseInt(challengeId);
				}

				if (iChallengeId > 0) {
					user = (User) request.getSession(false).getAttribute("user");

					LOG.info("User Login Control, Authorization check for Challenge Quiz:" + user.getMailId());
					isAuthorized = questionService.authorizationCheckBychallengeId(iChallengeId, user.getMailId());
				}

				if (!isAuthorized && iChallengeId > 0) {
					modelAndView.setViewName("UnauthorizeQuizAccess");
				} else {
					modelAndView.setViewName("home");
				}
				return modelAndView;
			}
		}
		modelAndView.setViewName("relogin");
		return modelAndView;

	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView viewProfile(@ModelAttribute User user, HttpServletRequest request) throws Exception {
		user = (User) httpSession.getAttribute("user");
		LOG.info("View Profile");
		LOG.info("Email: " + user.getMailId());
		LOG.info("Role: " + user.getRole());
		ModelAndView modelAndView = new ModelAndView();
		if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {
			modelAndView.addObject("user", user);
			modelAndView.setViewName("profile");
		} else {
			modelAndView.setViewName("session-expried-or-null");
		}
		return modelAndView;
	}

	@RequestMapping("/update")
	public ModelAndView updateUser(@ModelAttribute User user, @RequestParam String oldpassword,
			@RequestParam String password, @RequestParam String repassword, HttpServletRequest request)
			throws Exception {

		user = (User) httpSession.getAttribute("user");
		ModelAndView modelAndView = new ModelAndView();

		// encoding pass enterd by user to varify login
		Base64.Encoder encoder = Base64.getEncoder();
		String normalString = oldpassword;
		String encodedString = encoder.encodeToString(normalString.getBytes(StandardCharsets.UTF_8));
		oldpassword = encodedString;

		if (user.getPassword().equals(oldpassword)) {
			if (password.equals(repassword)) {
				LOG.info("Profile is successfully updated");
				LOG.info("Email: " + user.getMailId());
				LOG.info("Role: " + user.getRole());
				user.setPassword(password);
				userService.update(user);
				if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {
					List<Map<String, Object>> categoryList = userService.getAllCategories();
					request.setAttribute("categoryList", categoryList);
					modelAndView.setViewName("update");
				} else {
					modelAndView.setViewName("session-expried-or-null");
				}
			} else {
				modelAndView.setViewName("updatefailed");
			}

		} else {
			modelAndView.setViewName("updatefailed2");
		}
		return modelAndView;

	}

	@GetMapping("/logout")
	public String logout(HttpServletRequest req) {
		httpSession = req.getSession(false);
		if (httpSession != null) {
			httpSession.invalidate();
		}

		return "index";
	}

	@RequestMapping("/home")
	public ModelAndView loginHome(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {
			List<Map<String, Object>> categoryList = userService.getAllCategories();
			request.setAttribute("categoryList", categoryList);
			httpSession.removeAttribute("questionList");
			if (httpSession.getAttribute("selectedValue") != null) {
				httpSession.removeAttribute("selectedValue");
			}
			modelAndView.setViewName("home");
		} else {
			modelAndView.setViewName("session-expried-or-null");
		}
		return modelAndView;
	}

	@RequestMapping("/result")
	public ModelAndView result(@ModelAttribute TestResult testResult, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		if (request.getSession(false) != null && request.getSession(false).getAttribute("user") != null) {
			List<TestResult> resultList = userService.getAllResult();
			modelAndView.addObject("resultList", resultList);
			modelAndView.setViewName("result");
		} else {
			modelAndView.setViewName("session-expried-or-null");
		}
		return modelAndView;
	}

}