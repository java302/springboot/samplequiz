package com.coforge.service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.coforge.entities.Question;

@Service
public class FileUploaderServiceImpl implements IFileUploaderService {

	private static final Logger LOG = LoggerFactory.getLogger(FileUploaderServiceImpl.class);

	public List<Question> invoiceExcelReaderService() {
		return null;
	}

	public String uploadDir = System.getProperty("java.io.tmpdir");

	public void uploadFile(MultipartFile file) {

		try {
			Path copyLocation = Paths
					.get(uploadDir + File.separator + StringUtils.cleanPath(file.getOriginalFilename()));
			Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);
			LOG.info("Path to copy the file : " + copyLocation);
		} catch (Exception e) {
			LOG.error("UNexpected Error :" + e);
			throw new RuntimeException("Could not store file " + file.getOriginalFilename() + ". Please try again!");
		}
	}
}