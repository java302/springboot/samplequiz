package com.coforge.service;

import java.util.List;

import com.coforge.entities.Question;

public interface IExcelDataService {

	List<Question> getExcelDataAsList(String file);

}