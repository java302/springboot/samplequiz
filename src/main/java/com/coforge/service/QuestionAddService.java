package com.coforge.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coforge.daos.QuestionAddDao;
import com.coforge.entities.Category;
import com.coforge.entities.Question;

@Service
public class QuestionAddService {

	private static final Logger LOG = LoggerFactory.getLogger(QuestionAddService.class);

	@Autowired
	private QuestionAddDao questionAddDao;

	public boolean saveQuestion(Question question) {
		boolean status = false;
		try {
			status = questionAddDao.saveQuestion(question);
		} catch (Exception e) {
			LOG.error("Unexpected Error : " + e);
		}
		return status;
	}

	public List<Map<String, Object>> getAllCategories() {
		List<Map<String, Object>> category = null;
		try {
			category = questionAddDao.getAllCategories();
		} catch (Exception e) {
			LOG.error("Unexpected Error : " + e);
		}
		return category;
	}

	public boolean saveCategory(Category category) {
		boolean status = false;
		try {
			status = questionAddDao.saveCategory(category);
		} catch (Exception e) {
			LOG.error("Unexpected Error : " + e);
		}
		return status;

	}

	public String getQuestionCount() {
		String qCode = null;
		try {
			int count = questionAddDao.getQuestionCount();
			count = count + 1;
			qCode = "Q" + count;
		} catch (Exception e) {
			LOG.error("Unexpected Error : " + e);
		}
		return qCode;

	}

	public String getOptionCount() {
		String opCode = null;
		try {
			int count = questionAddDao.getOptionCount();
			count = count + 1;
			opCode = "OP" + count;
		} catch (Exception e) {
			LOG.error("Unexpected Error : " + e);
		}
		return opCode;
	}

	public String getAnswerCount() {
		String ansCode = null;
		try {
			int count = questionAddDao.getAnswerCount();
			count = count + 1;
			ansCode = "A" + count;
		} catch (Exception e) {
			LOG.error("Unexpected Error : " + e);
		}
		return ansCode;
	}

	public boolean saveOptions(Question question) {
		boolean status = false;
		try {
			status = questionAddDao.saveOptions(question);
		} catch (Exception e) {
			LOG.error("Unexpected Error : " + e);
		}
		return status;

	}

	public boolean saveAnswer(Question question) {
		boolean status = false;
		try {
			status = questionAddDao.saveAnswer(question);
		} catch (Exception e) {
			LOG.error("Unexpected Error : " + e);
		}
		return status;

	}

	public boolean checkQuestion(String questionName) {
		boolean status = false;
		try {
			status = questionAddDao.checkQuestion(questionName);
		} catch (Exception e) {
			LOG.error("Unexpected Error : " + e);
		}
		return status;

	}

	public boolean saveQuestionFromExcel(List<Question> excelDataAsList) {

		boolean status = false;
		try {
			status = questionAddDao.saveQuestionFromExcel(excelDataAsList);
		} catch (Exception e) {
			LOG.error("Unexpected Error : " + e);
		}
		return status;

	}

}
