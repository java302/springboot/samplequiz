package com.coforge.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coforge.daos.QuestionDao;
import com.coforge.entities.Question;
import com.coforge.entities.QuizChallengeDetails;
import com.coforge.entities.TestResult;

@Service
public class QuestionService {

	private static final Logger LOG = LoggerFactory.getLogger(QuestionService.class);

	@Autowired
	private QuestionDao questionDao;

	public List<Question> findRandomQuesByCategory(String categoryId, int quesMaxLimit) {
		List<Question> questions = questionDao.getQuestions(categoryId, quesMaxLimit);
		return questions;
	}

	public String getCategoryName(String categoryCode) {
		String categoryName = null;
		try {
			categoryName = questionDao.getCategoryName(categoryCode);
		} catch (Exception e) {
			LOG.error("Unexpected Error : "+e);
		}
		return categoryName;
	}

	public void saveResult(TestResult testResult) {
		try {
			questionDao.saveResult(testResult);
		} catch (Exception e) {
			LOG.error("Unexpected Error : "+e);
		}
	}
	
	public void saveChallengeResult(List<QuizChallengeDetails> quizList, String category) {
		try {
			questionDao.saveChallengeResult(quizList, category);
		} catch (Exception e) {
			LOG.error("Unexpected Error : "+e);
		}
	}
	
	public List<Question> findQuestionsDetailsForChallengeID(String categoryId, String challengeId) {
		List<Question> questions = questionDao.findQuestionsDetailsForChallengeID(categoryId, challengeId);
		return questions;
	}
	
	public List<QuizChallengeDetails> saveChallengeContestantDetails(int challengeId, String emailIds)
			throws Exception {
		List<QuizChallengeDetails> challengeContDetList = null;
		try {
			challengeContDetList = questionDao.saveChallengeContestantDetails(challengeId, emailIds);
		} catch (Exception e) {
			LOG.error("Unexpected Error : "+e);
		}
		return challengeContDetList;
	}
	
	public boolean authorizationCheckBychallengeId(int challengeId, String loginUserEmailId) throws Exception {
		boolean isAuthorized = questionDao.authorizationCheckBychallengeId(challengeId, loginUserEmailId);
		return isAuthorized;
	}

	public int saveChallenge(List<QuizChallengeDetails> quizChallengeDetList, String category) {
		int challengeId = 0;
		try {
			challengeId = questionDao.saveChallengeResult(quizChallengeDetList, category);
		} catch (Exception e) {
			LOG.error("Unexpected Error : "+e);
		}

	
		return challengeId;
	}

	public List<Question> getQuesByCategory(String selectdCategory) {
		List<Question> questions = questionDao.findQuestionsByCategory(selectdCategory);
		return questions;
	}

	public List<Question> getQuestionsFromQCode(String[] qList) {
		List<Question> questions = questionDao.getQuestionsFromQCode(qList);
		return questions;
	}	
}
