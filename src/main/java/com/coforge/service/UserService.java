package com.coforge.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coforge.daos.UserDao;
import com.coforge.entities.Question;
import com.coforge.entities.TestResult;
import com.coforge.entities.User;

@Service
public class UserService {

	private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private UserDao userDao;

	public void save(User user) {
		try {
			userDao.save(user);
		} catch (Exception e) {
			LOG.error("Unexpected Error : "+e);
		}
	}

	public void update(User user) {
		try {
			userDao.update(user);
		} catch (Exception e) {
			LOG.error("Unexpected Error : "+e);
		}

	}

	public User findByMailId(String mailId) {
		List<User> userList = new ArrayList<User>();
		User user = new User();
		try {
			userList = userDao.findByMailId(mailId);
			if (userList.size() > 0) {
				user = userList.get(0);
			} else {
				user = null;
			}

		} catch (Exception e) {
			LOG.error("Unexpected Error : "+e);
		}
		return user;
	}
	
	public User findByName(String name) {
		List<User> userList = new ArrayList<User>();
		User user = new User();
		try {
			userList = userDao.findByName(name);
			if (userList.size() > 0) {
				user = userList.get(0);
			} else {
				user = null;
			}

		} catch (Exception e) {
			LOG.error("Unexpected Error : "+e);
		}
		return user;
	}

	public List<Question> findRandomQuesByCategory(String categoryId) {
		List<Question> questions = userDao.getQuestions(categoryId);
		return questions;
	}

	public Question findAnswerByQuestion(String questionId) {
		Question questions = userDao.getAnsByQuestion(questionId);
		return questions; // TODO Auto-generated method stub
	}

	public List<Map<String, Object>> getAllCategories() {
		List<Map<String, Object>> category = null;
		try {
			category = userDao.getAllCategories();
		} catch (Exception e) {
			LOG.error("Unexpected Error : "+e);

		}
		return category;
	}

	public String getCategoryName(String categoryCode) {
		String categoryName = null;
		try {
			categoryName = userDao.getCategoryName(categoryCode);
		} catch (Exception e) {
			LOG.error("Unexpected Error : "+e);
		}
		return categoryName;
	}
	
	public void saveResult(TestResult testResult) {
		try {
			userDao.saveResult(testResult);
		} catch (Exception e) {
			LOG.error("Unexpected Error : "+e);
		}
	}
	
	public List<TestResult> getAllResult() {
		List<TestResult> result = null;
		try {
			result = userDao.getAllResult();
		} catch (Exception e) {
			LOG.error("Unexpected Error : "+e);

		}
		return result;
	}
}
