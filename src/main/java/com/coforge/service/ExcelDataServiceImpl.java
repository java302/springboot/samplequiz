package com.coforge.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.coforge.entities.Question;

@Service
public class ExcelDataServiceImpl implements IExcelDataService {

	private static final Logger LOG = LoggerFactory.getLogger(ExcelDataServiceImpl.class);

	public String excelFilePath = System.getProperty("java.io.tmpdir");
	public String fullFilePath;

	Workbook workbook;

	public List<Question> getExcelDataAsList(String file) {

		List<String> list = new ArrayList<String>();

		// Create a DataFormatter to format and get each cell's value as String
		DataFormatter dataFormatter = new DataFormatter();

		// Create the Workbook
		try {
			fullFilePath = excelFilePath + file;
			LOG.info("Complete file path : " + fullFilePath);
			workbook = WorkbookFactory.create(new File(fullFilePath));
		} catch (EncryptedDocumentException | IOException e) {
			LOG.error("Unexpected Error : " + e);
		}

		// Retrieving the number of sheets in the Workbook
		LOG.info("Workbook has '" + workbook.getNumberOfSheets() + "' Sheets");
		// Getting the Sheet at index zero
		Sheet sheet = workbook.getSheetAt(0);

		// Getting number of columns in the Sheet
		int noOfColumns = sheet.getRow(0).getLastCellNum();

		LOG.info("Sheet has '" + noOfColumns + "' columns");

		// Using for-each loop to iterate over the rows and columns
		for (Row row : sheet) {
			for (Cell cell : row) {
				String cellValue = dataFormatter.formatCellValue(cell);
				list.add(cellValue);
			}
		}

		// filling excel data and creating list as List<Invoice>
		List<Question> invList = createList(list, noOfColumns);

		// Closing the workbook
		try {
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return invList;
	}

	private List<Question> createList(List<String> excelData, int noOfColumns) {

		ArrayList<Question> quesList = new ArrayList<Question>();

		int i = noOfColumns;
		while (i < excelData.size()) {
			Question ques = new Question();
			List<String> option = new ArrayList<String>();
			ques.setCategoryCode(excelData.get(i));
			ques.setQuestionName(excelData.get(i + 1));
			option.add((excelData.get(i + 2) != null) ? excelData.get(i + 2) : null);
			option.add((excelData.get(i + 3) != null) ? excelData.get(i + 3) : null);
			option.add((excelData.get(i + 4) != null) ? excelData.get(i + 4) : null);
			option.add((excelData.get(i + 5) != null) ? excelData.get(i + 5) : null);
			ques.setCorrectOption(excelData.get(i + 6));
			ques.setOptions(option);
			quesList.add(ques);
			i = i + (noOfColumns);
		}
		return quesList;
	}

}