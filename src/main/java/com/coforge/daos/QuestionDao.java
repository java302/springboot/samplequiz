package com.coforge.daos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.coforge.entities.Option;
import com.coforge.entities.Question;
import com.coforge.entities.QuizChallengeDetails;
import com.coforge.entities.TestResult;

@Repository
public class QuestionDao {

	private static final Logger LOG = LoggerFactory.getLogger(QuestionDao.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public static final String findQuestionsByCategory = "SELECT c.CATEGORYNAME, q.QUESTIONCODE, q.QUESTIONNAME, o.OPTIONCODE, o.OPTION, a.ANSWERCODE "
			+ "FROM quiz.category c, quiz.question q, quiz.options o, quiz.answers a  "
			+ "WHERE c.CATEGORYCODE = q.CATEGORYCODE AND q.QUESTIONCODE = o.QUESTIONCODE AND o.OPTIONCODE = a.OPTIONCODE AND c.CATEGORYCODE=? "
			+ "ORDER BY RAND() LIMIT ?";

	public static final String findOptionsByQuestion = " SELECT o.OPTIONCODE, o.OPTION "
			+ "FROM question q, options o "
			+ "WHERE q.QUESTIONCODE = o.QUESTIONCODE and q.QUESTIONCODE = ? ";

	public static final String get_category_name = "select categoryname from category where categorycode = ?";

	public static final String save_result_query = "insert into testresult (name, mailid, category, marks, date) values(?, ?, ?, ?, ?)";

	public static final String create_challenge_details = "insert into QUIZ_CHALLENGE_DETAILS (CHALLENGE_DATE, CATEGORYCODE) values(?, ?)";

	public static final String get_last_insert_id = "SELECT LAST_INSERT_ID()";

	public static final String save_challenge_details = "insert into quiz_challenge (challenge_id, quiz_id, question_code) values(?, ?, ?)";

	public static final String findQuestionsDetailsForChallengeID = "SELECT c.CATEGORYNAME, q.QUESTIONCODE,q.QUESTIONNAME,o.OPTIONCODE, o.OPTION, a.ANSWERCODE"
			+ " FROM quiz.category c, quiz.question q, quiz.options o, quiz.answers a"
			+ " WHERE c.CATEGORYCODE = q.CATEGORYCODE AND q.QUESTIONCODE = o.QUESTIONCODE AND o.OPTIONCODE = a.OPTIONCODE"
			+ " AND c.CATEGORYCODE = ? AND q.QUESTIONCODE IN (SELECT ch.QUESTION_CODE FROM quiz_challenge ch WHERE ch.CHALLENGE_ID = ?)"
			+ " ORDER BY RAND() LIMIT 10 ";

	public static final String create_challenge_contestant_details = "insert into CHALLENGE_CONTESTANT_DET (CON_EMAIL_ID, CON_CHALLENGE_ID, CON_CHALLENGE_DATE) "
			+ "values(?, ?, ?)";

	public static final String sql_contestant_auth_Check = "SELECT CON_CHALLENGE_ID FROM challenge_contestant_det "
			+ "WHERE CON_CHALLENGE_ID = ? AND CON_EMAIL_ID = ? GROUP BY CON_CHALLENGE_ID";

	public static final String getQuestionsByCategory = "SELECT QUESTIONCODE, QUESTIONNAME FROM question where CATEGORYCODE = ? ORDER BY RAND() LIMIT 20";

	public static final String getQuestionDataByQCode = "SELECT * FROM question where QUESTIONCODE = ?";
	RowMapper<Question> mapQuestions = (rs, rowNum) -> {

		Question ques = new Question();
		ques.setCategoryName(rs.getString(1));
		ques.setQuestionCode(rs.getString(2));
		ques.setQuestion(rs.getString(3));
		ques.setOptionCode(rs.getString(4));
		ques.setOption(rs.getString(5));
		ques.setAnsCode(rs.getString(6));
		return ques;

	};

	RowMapper<Question> mapQuestionsForChallenge = (rs, rowNum) -> {

		Question ques = new Question();
		ques.setQuestionCode(rs.getString(1));
		ques.setQuestion(rs.getString(2));
		return ques;

	};
	
	RowMapper<Question> mapQuestionsByQCode = (rs, rowNum) -> {

		Question ques = new Question();
		ques.setQuestionCode(rs.getString(1));
		ques.setCategoryCode(rs.getString(2));
		ques.setQuestion(rs.getString(3));
		return ques;

	};
	RowMapper<Option> mapOptions = (rs, rowNum) -> {

		Option option = new Option();
		option.setOptionCode(rs.getString(1));
		option.setOption(rs.getString(2));
		return option;

	};

	public List<Question> getQuestions(String categoryCode, int quesMaxLimit) {
		List<Question> questions = new ArrayList<Question>();
		questions = jdbcTemplate.query(findQuestionsByCategory, new Object[] { categoryCode, quesMaxLimit }, mapQuestions);
		if (!questions.isEmpty()) {
			for (Question question : questions) {
				String answer = null;
				String questionCode = question.getQuestionCode();
				List<Option> optionList = jdbcTemplate.query(findOptionsByQuestion, new String[] { questionCode },
						mapOptions);
				for (Option option : optionList) {
					if (!StringUtils.isEmpty(option) && option.getOptionCode().equals(question.getOptionCode())) {
						answer = option.getOption();
					}
				}
				question.setAns(answer);
				question.setOptionsList(optionList);
			}
		}
		return questions;
	}

	public String getCategoryName(String categoryCode) throws Exception {
		String categoryName = jdbcTemplate.queryForObject(get_category_name, new Object[] { categoryCode }, String.class);
		return categoryName;
	}

	public void saveResult(TestResult testResult) throws Exception {
		jdbcTemplate.update(save_result_query, testResult.getName(), testResult.getMailId(), testResult.getCategory(),
				testResult.getMarks(), new Date());
	}

	public int saveChallengeResult(List<QuizChallengeDetails> quizList, String category) throws Exception {
		QuizChallengeDetails quizChallengeDetails = null;

		jdbcTemplate.update(create_challenge_details, new Date(), category);

		Integer challengeId = jdbcTemplate.queryForObject(get_last_insert_id, Integer.class);
		
		LOG.info("2. create_challenge_details_id - " + challengeId);

		for (int i = 0; i < quizList.size(); i++) {
			quizChallengeDetails = quizList.get(i);
			jdbcTemplate.update(save_challenge_details, challengeId, quizChallengeDetails.getQuizId(),
					quizChallengeDetails.getQuestionCode());
		}
		return challengeId;
	}

	public List<Question> findQuestionsDetailsForChallengeID(String categoryCode, String challengeId) {
		List<Question> questions = new ArrayList<Question>();
		questions = jdbcTemplate.query(findQuestionsDetailsForChallengeID, new String[] { categoryCode, challengeId },
				mapQuestions);
		if (!questions.isEmpty()) {
			for (Question question : questions) {
				String answer = null;
				String questionCode = question.getQuestionCode();
				List<Option> optionList = jdbcTemplate.query(findOptionsByQuestion, new String[] { questionCode },
						mapOptions);
				for (Option option : optionList) {
					if (!StringUtils.isEmpty(option) && option.getOptionCode().equals(question.getOptionCode())) {
						answer = option.getOption();
					}
				}
				question.setAns(answer);
				question.setOptionsList(optionList);
			}
		}
		return questions;
	}

	public List<QuizChallengeDetails> saveChallengeContestantDetails(int challengeId, String emailIds)
			throws Exception {
		Integer contestantId = 0;
		List<QuizChallengeDetails> challengeContDetList = new ArrayList<QuizChallengeDetails>();
		if (null != emailIds) {
			String arrEmailId[] = emailIds.split(",");
			for (int i = 0; i < arrEmailId.length; i++) {
				jdbcTemplate.update(create_challenge_contestant_details, arrEmailId[i], challengeId, new Date());
				contestantId = jdbcTemplate.queryForObject(get_last_insert_id, Integer.class);
				LOG.info("Contestant Id - " + contestantId);
				
				QuizChallengeDetails challengeContDet = new QuizChallengeDetails();
				challengeContDet.setChallengeId(challengeId);
				challengeContDet.setContestantId(String.valueOf(contestantId));
				challengeContDet.setEmailId(arrEmailId[i]);
				challengeContDetList.add(challengeContDet);
			}
		} else {
			throw new NullPointerException("Error: email id details missing");
		}
		return challengeContDetList;
	}

	public boolean authorizationCheckBychallengeId(int challengeId, String loginUserEmailId) throws Exception {
		boolean isAuthorized = false;
		try {
			Integer contestantId = jdbcTemplate.queryForObject(sql_contestant_auth_Check,
					new Object[] { String.valueOf(challengeId), loginUserEmailId }, Integer.class);
			if (contestantId > 0) {
				isAuthorized = true;
			}
		} catch (EmptyResultDataAccessException e) {
			LOG.info("No record found in database, Quiz Challenge authorization failed.");
		}

		return isAuthorized;
	}

	public List<Question> findQuestionsByCategory(String categoryCode) {
		List<Question> questions = new ArrayList<Question>();
		questions = jdbcTemplate.query(getQuestionsByCategory, new String[] { categoryCode }, mapQuestionsForChallenge);

		return questions;
	}

	public List<Question> getQuestionsFromQCode(String[] qList) {
		List<Question> questionList = new ArrayList<Question>();
		Question question = new Question();
		for (String qCode : qList) {
			question = jdbcTemplate.queryForObject(getQuestionDataByQCode, new String[] { qCode }, mapQuestionsByQCode);
			questionList.add(question);
		}
		return questionList;
	}
}
