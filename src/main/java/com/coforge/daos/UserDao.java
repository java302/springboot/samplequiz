package com.coforge.daos;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.coforge.entities.Option;
import com.coforge.entities.Question;
import com.coforge.entities.TestResult;
import com.coforge.entities.User;

@Repository
public class UserDao {

	@Autowired
	private JdbcTemplate template;

	public static final String save_query = "insert into userlogin (name,mailId,password,role,date) values(?,?,?,?,?)";

	public static final String findByMailId_query = "select * from userlogin where mailId=?";

	public static final String findByName_query = "select * from userlogin where name=?";

	public static final String update_query = "update userlogin set name=?,mailId=?,password=?,role=? where id=?";

	public static final String findQuestionsByCategory = "SELECT c.CATEGORYNAME, q.QUESTIONCODE,q.QUESTIONNAME,o.OPTIONCODE,o.OPTION,a.ANSWERCODE FROM cfg.category c,cfg.question q ,cfg.options o ,cfg.answers a  WHERE c.CATEGORYCODE=q.CATEGORYCODE AND q.QUESTIONCODE=o.QUESTIONCODE AND o.OPTIONCODE= a.OPTIONCODE AND c.CATEGORYCODE=? ORDER BY RAND() LIMIT 10";

	public static final String findOptionsByQuestion = " SELECT o.OPTIONCODE, o.OPTION FROM question q, options o WHERE q.QUESTIONCODE=o.QUESTIONCODE and q.QUESTIONCODE=? ";

	public static final String findAnswerByQuestion = "SELECT q.QUESTIONNAME,o.OPTIONCODE,o.OPTION,a.ANSWERCODE FROM question q ,options o ,answers a WHERE q.QUESTIONCODE=o.QUESTIONCODE AND o.OPTIONCODE= a.OPTIONCODE AND q.QUESTIONCODE=?";

	public static final String get_categories_query = "select * from category";

	public static final String get_category_name = "select categoryname from category where categorycode=?";

	public static final String save_result_query = "insert into testresult (name,mailid,category,marks,date) values(?,?,?,?,?)";

	public static final String get_result_query = "select * from testresult";

	public void save(User user) throws Exception {
		Base64.Encoder encoder = Base64.getEncoder();
		String normalString = user.getPassword();
		String encodedString = encoder.encodeToString(normalString.getBytes(StandardCharsets.UTF_8));
		String password = encodedString;
		template.update(save_query, user.getName(), user.getMailId().toLowerCase(), password, user.getRole(),
				new Date());
	}

	public List findByMailId(String mailId) throws Exception {
		return template.query(findByMailId_query, new String[] { mailId.toLowerCase() }, mapRow);
	}

	public List findByName(String name) throws Exception {
		return template.query(findByName_query, new String[] { name }, mapRow);
	}

	RowMapper<User> mapRow = (rs, rowNum) -> {

		User user = new User();
		user.setId(rs.getInt(1));
		user.setName(rs.getString(2));
		user.setMailId(rs.getString(3));
		user.setPassword(rs.getString(4));
		user.setRole(rs.getString(6));
		return user;

	};

	public void update(User user) throws Exception {
		Base64.Encoder encoder = Base64.getEncoder();
		String normalString = user.getPassword();
		String encodedString = encoder.encodeToString(normalString.getBytes(StandardCharsets.UTF_8));
		String password = encodedString;
		template.update(update_query, user.getName(), user.getMailId(), password, user.getRole(), user.getId());
	}

	RowMapper<Question> mapAnswers = (rs, rowNum) -> {

		Question ques = new Question();
		ques.setQuestion(rs.getString(1));
		ques.setOptionCode(rs.getString(2));
		ques.setOption(rs.getString(3));
		ques.setAnsCode(rs.getString(4));
		return ques;

	};

	RowMapper<Question> mapQuestions = (rs, rowNum) -> {

		Question ques = new Question();
		ques.setCategoryName(rs.getString(1));
		ques.setQuestionCode(rs.getString(2));
		ques.setQuestion(rs.getString(3));
		ques.setOptionCode(rs.getString(4));
		ques.setOption(rs.getString(5));
		ques.setAnsCode(rs.getString(6));
		return ques;

	};

	RowMapper<Option> mapOptions = (rs, rowNum) -> {

		Option option = new Option();
		option.setOptionCode(rs.getString(1));
		option.setOption(rs.getString(2));
		return option;

	};

	public List<Question> getQuestions(String categoryCode) {
		List<Question> questions = new ArrayList<Question>();
		questions = template.query(findQuestionsByCategory, new String[] { categoryCode }, mapQuestions);
		if (!questions.isEmpty()) {
			for (Question question : questions) {
				String answer = null;
				String questionCode = question.getQuestionCode();
				List<Option> optionList = template.query(findOptionsByQuestion, new String[] { questionCode },
						mapOptions);
				for (Option option : optionList) {
					if (!StringUtils.isEmpty(option) && option.getOptionCode().equals(question.getOptionCode())) {
						answer = option.getOption();
					}
				}
				question.setAns(answer);
				question.setOptionsList(optionList);
			}
		}
		return questions;
	}

	public Question getAnsByQuestion(String QuestionCode) {
		List<Question> questions = new ArrayList<Question>();
		Question question = null;
		questions = template.query(findAnswerByQuestion, new String[] { QuestionCode }, mapAnswers);
		if (!questions.isEmpty() && questions.get(0) != null) {
			question = questions.get(0);
		}
		return question;
	}

	public List<Map<String, Object>> getAllCategories() {
		List<Map<String, Object>> categories = template.queryForList(get_categories_query);
		return categories;
	}

	public String getCategoryName(String categoryCode) throws Exception {
		String categoryName = template.queryForObject(get_category_name, new Object[] { categoryCode }, String.class);
		return categoryName;
	}

	public void saveResult(TestResult testResult) throws Exception {
		template.update(save_result_query, testResult.getName(), testResult.getMailId(), testResult.getCategory(),
				testResult.getMarks(), new Date());
	}

	public List<TestResult> getAllResult() {
		List<TestResult> result = template.query(get_result_query, new String[] {},
				(rs, rowNum) -> new TestResult(rs.getInt("id"), rs.getString("name"), rs.getString("mailId"),
						rs.getString("category"), rs.getString("marks"), rs.getString("date")));
		return result;
	}
}