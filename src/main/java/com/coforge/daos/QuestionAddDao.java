package com.coforge.daos;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.coforge.entities.Category;
import com.coforge.entities.Question;
import com.coforge.service.QuestionAddService;

@Repository
public class QuestionAddDao {

	private static final Logger LOG = LoggerFactory.getLogger(QuestionAddDao.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private QuestionAddService questionAddService;

	// count the total record in question table
	public static final String get_question_count = "select count(*) from question";

	// count the total record in option table
	public static final String get_option_count = "select count(*) from options";

	// count the total record in answer table
	public static final String get_answer_count = "select count(*) from answers";

	// Query to save a question
	public static final String save_question_query = "insert into question (questioncode,categorycode,questionname) values(?,?,?)";

	// Query to get Categories
	public static final String get_categories_query = "select * from category";

	// Query to save a new category
	public static final String save_category_query = "insert into category (categorycode,categoryname) values(?,?)";

	// Query to save in options table
	public static final String save_options_query = "insert into options (optioncode, questioncode, `option`) values(?,?,?)";

	// Query to get answer
	public static final String get_question_query = "select questioncode, questionname from question";

	// Query to get options by questionCode
	public static final String get_options_query = "select optioncode,`option` from options where questionCode=?";

	// Query to save in answer table
	public static final String save_answer_query = "insert into answers (answercode,questioncode,optioncode) values(?,?,?)";

	// Query to select all the data from answers table
	public static final String get_answers_query = "select questioncode from answers";

	// Query to select all the data from answers table
	public static final String get_optioncode_answer_query = "select  count(*) from answers where questioncode=?";

	// Query to update option in answers table
	public static final String update_answer_query = "update answers set optionCode = ? where questionCode = ?";

	// Query to select all the data from category table
	public static final String check_Category_query = "select  count(*) from category where categorycode=?";

	// Query to get options by questionCode
	public static final String get_questions_query = "select questioncode,questionName from question where categorycode=?";

	public static final String checkQuestion = "select  count(*) from question where questionname = ?";

	public static final String getQuestionCode = "select  count(*) from question where questioncode = ?";

	public static final String update_question = "update question set categorycode = ?, questionname = ? where questioncode = ?";

	// Method to get questionCount
	public int getQuestionCount() throws Exception {
		int count = jdbcTemplate.queryForObject(get_question_count, new Object[] {}, Integer.class);
		return count;

	}

	// Method to get optionCount
	public int getOptionCount() throws Exception {
		int count = jdbcTemplate.queryForObject(get_option_count, new Object[] {}, Integer.class);
		return count;
	}

	// Method to get answerCount
	public int getAnswerCount() throws Exception {
		int count = jdbcTemplate.queryForObject(get_answer_count, new Object[] {}, Integer.class);
		return count;
	}

	// Method to save a question
	public boolean saveQuestion(Question question) throws Exception {
		boolean status = false;
		String questionName = question.getQuestionName();
		if (question.getQuestionCode() != null && question.getCategoryCode() != null && questionName != null) {
			questionName = questionName.replaceAll("(?:<)(?<=<)(\\/?\\w*)(?:>)", "&lt;$1&gt;");
			int count = jdbcTemplate.update(save_question_query, question.getQuestionCode(), question.getCategoryCode(),
					questionName.trim());
			if (count > 0) {
				status = true;
			} else {
				status = false;
			}
		}
		return status;
	}

	public List<Map<String, Object>> getAllCategories() {
		List<Map<String, Object>> categories = jdbcTemplate.queryForList(get_categories_query);
		return categories;
	}

	// Method to save new category
	public boolean saveCategory(Category category) {
		int action = 0;
		boolean status = false;
		int count = jdbcTemplate.queryForObject(check_Category_query, new Object[] { category.getCategoryCode() },
				Integer.class);

		if (count > 0) {
			LOG.info("Category already exist in the category table.");
			status = false;
		} else {
			action = jdbcTemplate.update(save_category_query, category.getCategoryCode(), category.getCategoryName());
			if (action > 0) {
				status = true;
				LOG.info(category.getCategoryName() + " saved in category table.");
			} else {
				status = false;
				LOG.info("Error saving " + category.getCategoryName() + " in category table.");
			}
		}

		return status;

	}

	// Method to save options
	public boolean saveOptions(Question question) {
		boolean status = false;
		if (!question.getOptions().isEmpty() || question.getOptions() != null) {
			List<String> optionsList = question.getOptions();
			for (String op : optionsList) {
				if (!op.equals("null") && !op.isEmpty()) {
					op = op.replaceAll("(?:<)(?<=<)(\\/?\\w*)(?:>)", "&lt;$1&gt;");
					question.setOptionCode(questionAddService.getOptionCount());
					int count = jdbcTemplate.update(save_options_query, question.getOptionCode(),
							question.getQuestionCode(), op);
					if (question.getCorrectOption().equals(op)) {
						question.setAns(question.getOptionCode());
					}
					if (count > 0) {
						status = true;
						LOG.info("Options saved into the Options table.");
					} else {
						status = false;
						LOG.info("Error in saving options in Options table.");
					}
				}
			}
		}
		return status;
	}

	// method to save in answers table
	public boolean saveAnswer(Question question) {

		// get optionCode
		jdbcTemplate.queryForObject(get_optioncode_answer_query, new Object[] { question.getQuestionCode() },
				Integer.class);
		boolean status = false;

		// data insert into answers table
		int c = jdbcTemplate.update(save_answer_query, question.getAnswerCode(), question.getQuestionCode(),
				question.getAns());
		if (c > 0) {
			status = true;
			LOG.info("Answer saved in answer table.");
		} else {
			status = false;
			LOG.info("Error in saving answers in Answer table.");
		}
		// }
		return status;
	}

	// Method to check name
	public boolean checkQuestion(String questionName) {
		boolean status = false;
		if (questionName != null) {
			questionName = questionName.trim();
			int count = jdbcTemplate.queryForObject(checkQuestion, new Object[] { questionName }, Integer.class);
			if (count > 0) {
				return true;
			} else {
				return false;
			}
		}
		return status;
	}

	public boolean saveQuestionFromExcel(List<Question> excelDataAsList) {
		int count = 0;
		boolean status = false;
		String qcode = null;
		for (Question question : excelDataAsList) {
			// to check if question exist or not
			status = questionAddService.checkQuestion(question.getQuestionName());
			if (status != true) {
				// to create the question code dynamcily
				qcode = questionAddService.getQuestionCount();
				question.setQuestionCode(qcode);
				List<String> optionList = question.getOptions();
				int countNull = 0;
				for (String option : optionList) {

					if (option.equals("null")) {
						countNull = countNull + 1;
					}
				}
				if (countNull <= 2) {
					count = jdbcTemplate.update(save_question_query, qcode, question.getCategoryCode(),
							question.getQuestionName());
					if (count > 0) {
						// save data in options table
						status = questionAddService.saveOptions(question);
					}
					if (count > 0 && status == true) {
						// save data in answer table
						question.setAnswerCode(questionAddService.getAnswerCount());
						status = questionAddService.saveAnswer(question);
					}
				} else {
					LOG.info(question.getQuestionName()
							+ " - This question can not saved in the database as it contains less than two option values.");
				}

			} else {
				LOG.info(question.getQuestionName() + " - Question is already exist in the database.");
			}
			if (count > 0) {
				status = true;
			} else {
				status = false;
			}
		}
		return status;
	}

}
