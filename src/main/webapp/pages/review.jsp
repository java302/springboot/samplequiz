<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>
<title>Review</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<style>
.table {
    width: 70%;
    max-width: 100%;
    margin-bottom: 30px;
}
#css-serial {
	counter-reset: serial-number; /* Set the serial number counter to 0 */
}

#css-serial td:first-child:before {
	counter-increment: serial-number;
	/* Increment the serial number counter */
	content: counter(serial-number); /* Display the counter */
}
</style>

</head>

<%
String[] qList = request.getParameterValues("qList");
StringBuilder quizList = new StringBuilder();
for (int i = 0; i < qList.length; i++) {
	quizList.append(qList[i]);
	if (i != (qList.length - 1)) {
		quizList.append(",");
	}
}
%>

<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"> <img
					src="/resources/static/images/Coforge_Logo_Navbar.png"
					style="width: 50px;">
				</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">${user.name}</a></li>
				<li class="active"><a href="../home">Home</a></li>
				<%-- <li class="active"><a href="#">Category: ${categoryList.categoryName}</a></li> --%>
			</ul>
			<ul class="nav navbar-right"
				style="margin-left: auto; margin-right: 0px;">
				<li><a class="btn btn-danger navbar-btn"
					href="../logout">Logout</a></li>
			</ul>
		</div>
	</nav>

	<img src="/resources/static/images/Coforge_Logo_Background.png"
		alt="Coforge Ltd"
		style="margin-top: 80px; width: 250px; height: 60px; display: block; margin-left: auto; margin-right: auto;">


	<h2 align="center">
		<font><strong>Create Quiz</strong></font>
	</h2>

	<h2 align="center">
		<font><strong>Review Your Questions!</strong></font>
	</h2>


	<form method="post" action="/quiz/challengeSend" name="reviewForm">
		<table id="css-serial" align="center" class="table table-striped table-bordered table-sm">
			<tr>

			</tr>
			<thead>
				<th>Q.Num</th>
				<th>Questions</th>
			</thead>
			<c:forEach var="questions" begin="0" end="19" items="${questionList}"
				varStatus="loop">

				<tr>
					<td>&nbsp;</td>
					<td>${questions.question}</td>
				</tr>
				<input type="hidden"
			name="category" id="category" value="${questions.categoryCode}">
			</c:forEach>
		</table>
		<p></p>
		<input type="hidden" name="questionsList" id="questionsList"
			value="<%=quizList.toString()%>"> 
			

		<center>
			<input class="btn btn-primary" type=button value="Back"
				onCLick="history.back()"> <input class="btn btn-primary"
				type=submit value="Challenge Colleagues">
		</center>
	</form>
</body>
</html>