<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Category:${questionList.get(0).categoryName}</title>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/resources/static/css/questionBoard.css">
<script src="/resources/static/js/questionBoard.js"></script>
<script src="/resources/static/js/countdown.js"></script>

<style type="text/css">
form {
  text-align: center;
}</style>
<script type="text/javascript">
	function newTabaction() {
		sendInfo();
	}
	window.onblur = newTabaction;
</script>

<script>
	var request;
	var count = 0;
	var callIfGetInfoCnt = 0;
	var maxLimit = ${sMaxLimit};

	function sendInfo() {
		var url = "/quiz/trackUserMovment";

		if (window.XMLHttpRequest) {
			request = new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			request = new ActiveXObject("Microsoft.XMLHTTP");
		}
		try {
			request.onreadystatechange = getInfo;
			request.open("GET", url, true);
			request.send();
		} catch (e) {
			alert("Error occurred, Unable to connect to server.");
		}
	}

	function getInfo() {
		if (request.readyState == 4) {
			count = request.responseText;
			count = count.trim();
		}

		if (count > callIfGetInfoCnt) {
			if (!(count == (1 + maxLimit))) {
				alert("Warning "+ count +": You are not allowed to minimize window or open the new tab/window in between of the test!");
			}
			if (count > maxLimit) {
				document.getElementById('AutoSubmit').click();
			}
			callIfGetInfoCnt = count;
		}
	}
</script>
</head>

<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"> <img
					src="/resources/static/images/Coforge_Logo_Navbar.png"
					style="width: 50px;">
				</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">${sessionScope.user.name} </a></li>
				<%-- <li class="active"><a href="#">${sessionScope.question} </a></li> --%>
				<!-- <li class="active"><a href="../loginuser/home">Home</a></li> -->
				<li class="active"><a href="#">Category:

						${questionList.get(0).categoryName}</a></li>
				<li><a href="#">Remaining Time: <span id="time"></span></a></li>
			</ul>

			<ul class="nav navbar-right"
				style="margin-left: auto; margin-right: 0px;">
				<li><a class="btn1 btn-danger navbar-btn"
					href="../home" onclick="clearSession();">Quit</a></li>
			</ul>

		</div>
	</nav>

	<img src="/resources/static/images/Coforge_Logo_Background.png"
		alt="Coforge Ltd"
		style="margin-top: 60px; width: 250px; height: 60px;">


	<div class="container">
		<!-- <div class="center"> -->
			<form id="formDisable" >

				<div class="btn-group-vertical" align="center">

					<c:forEach var="j" begin="0" end="${ questionList.size() - 1 }">
						<a type="submit" class="button btn-primary"
							id="${ questionList.get(j).questionCode }"
							onclick="if(optionValidation(this)){window.location.href= '/quiz/getQuestions?categoryCode=${questionList.get(j).categoryId}&page=${j}' + '&challengeId=<%=request.getParameter("challengeId") %>'}">${j+1}</a>
						<c:if test="${questionList.get(j).selectedOption != null}">
							<script type="text/javascript">
								var selectedQues = '${ questionList.get(j).questionCode}';
								var opSelected = '${ questionList.get(j).selectedOption }';
								document.getElementById(selectedQues).disabled = true;
								document.getElementById(selectedQues).style.backgroundColor = "green";
							</script>
						</c:if>
					</c:forEach>
					<h2 style="margin-top: 20px;">Question
						${questionList.get(questions.pageId).nextPageId}:</h2>

					<c:forEach var="questions" begin="${questions.pageId}"
						end="${questions.pageId}" items="${questionList}" varStatus="loop">
						<button type="button" class="btn btn-primary" disabled="disabled"
							id="${ questionList.get(questions.pageId).questionCode }">${ questionList.get(questions.pageId).question }</button>
						<input type="hidden" name="categoryName"
							value="${questions.categoryName}">
						<c:forEach var="options"
							items="${questionList.get(questions.pageId).optionsList}">
							<%--  <td> ${options.optionCode} </td> --%>
							<button onclick="enable_disable(this)" type="button"
								class="btn btn-primary" name="${options.optionCode }"
								id="${options.optionCode}" value="${ options.option }">${ options.option }</button>
						</c:forEach>
						<br>
						<!--question section-->
						<input type="hidden" id="questionButton"
							value="${ questionList.get(questions.pageId).questionCode }">
						<input type="hidden" id="selectedButton"
							value="${ questionList.get(questions.pageId).selectedOption }">
						<input type="hidden" id="correctAnswer"
							value="${questionList.get(questions.pageId).option}">
						<c:if
							test="${ questionList.get(questions.pageId).selectedOption != null }">

							<input type="hidden" id="selectedOpCode"
								value="${ questionList.get(questions.pageId).selectedOptionCode}">
							<input type="hidden" id="currectOpCode"
								value="${ questionList.get(questions.pageId).optionCode}">

							<script type="text/javascript">
								var selectedOpCode = document
										.getElementById("selectedOpCode").value;
								var currectOpCode = document
										.getElementById("currectOpCode").value;

								if (selectedOpCode == currectOpCode) {
									document.getElementById(selectedOpCode).style.backgroundColor = "green";
									document.getElementById(currectOpCode).style.backgroundColor = "green";
								} else {
									document.getElementById(selectedOpCode).style.backgroundColor = "red";
									document.getElementById(currectOpCode).style.backgroundColor = "green";
								}
								var form = document
										.getElementById("formDisable");
								var allElements = form.elements;
								for (var i = 0, l = allElements.length; i < l; ++i) {
									// allElements[i].readOnly = true;
									allElements[i].disabled = true;

								}
							</script>
						</c:if>
						
						<!--answer section-->

						<input type="hidden" id="correct_ans"
							value="${questionList.get(questions.pageId).optionCode }">

						<c:if test="${questions.pageId <= questionList.size() - 2}">
							<a class="btn btn-primary"
								onclick="if(optionValidation(this)){window.location.href= '/quiz/getQuestions?categoryCode=${questions.categoryId}&page=${questions.nextPageId}'+'&challengeId=<%=request.getParameter("challengeId") %>'}"
								type="submit">Next</a>
							<a id="Submit"
								onclick="if(optionValidation(this)){window.location.href= '/quiz/submitQuiz?categoryCode=${questions.categoryId}'+'&challengeId=<%=request.getParameter("challengeId") %>'}">
							</a>
						</c:if>

						<a class="btn btn-primary" id="Submit"
							onclick="clearSession();if(optionValidation(this)){window.location.href= '/quiz/submitQuiz?categoryCode=${questions.categoryId}' + '&challengeId=<%=request.getParameter("challengeId") %>'}">Submit
							Quiz</a>

						<a id="AutoSubmit" onclick="clearSession();if(optionValidation(this)){window.location.href= '/quiz/submitQuiz?categoryCode=${questions.categoryId}'}"></a>

					</c:forEach>
					
				</div>
			</form>
		<!-- </div> -->
		<br> <br>

	</div>


</body>
</html>