<!DOCTYPE html>
<html lang="en">

<head>
<title>Change Password</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/resources/static/css/profile.css">
<script src="/resources/static/js/validation.js"></script>
</head>

<body>
	��
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"> <img
					src="/resources/static/images/Coforge_Logo_Navbar.png"
					style="width: 50px;">
				</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">${user.name}</a></li>
				<li class="active"><a href="../home">Home</a></li>
			</ul>
			<ul class="nav navbar-right"
				style="margin-left: auto; margin-right: 0px;">
				<li><a class="btn1 btn-danger navbar-btn"
					href="../logout">Logout</a></li>
			</ul>
		</div>
	</nav>

	<!-- <img src="/resources/static/images/Coforge_Logo_Background.png"
		alt="Coforge Ltd"
		style="margin-top: 60px; width: 250px; height: 60px;"> -->

	<div class="container">
		<div class="row">
			<div class="col">
				<form action="update" method="post">
					<h3>Profile</h3>
					<input type="hidden" name="id" value="${user.id}"> <input
						type="hidden" name="name" value="${user.name}"> <input
						type="hidden" name="mailId" value="${user.mailId}"> <input
						type="password" name="oldpassword" placeholder="Old password"
						required="required"> <input type="password"
						name="password" placeholder="New password" required="required">
					<input type="password" name="repassword"
						placeholder="Confirm password" required="required"> <!-- <label
						for="role">Role: </label> <select name="role" id="role" class="form-control">
						<option value="Admin" selected="selected">Admin</option>
						<option value="User">User</option>
					</select> -->
					<input type="hidden" name="role" value="User">
					<input type="submit" value="Update">
				</form>
			</div>
		</div>
	</div>


</body>
</html>