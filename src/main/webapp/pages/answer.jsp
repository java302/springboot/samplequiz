<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>
<title>Answer PAGE</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="stylesheet" href="/resources/static/css/category.css">
<script type="text/javascript">
	function changeCategory() {
		var selectBox = document.getElementById("categoryName");
		var selectedValue = selectBox.options[selectBox.selectedIndex].value;
		$(document).ready(
				function() {
					$.ajax({
						type : 'POST',
						url : 'http://localhost:8080/question/get-question',
						dataType : "json",
						data : {
							categoryCode : selectedValue
						},
						success : function(response) {
							
							$("#questionCode").empty();
							$('#questionCode').append(
									"<option value='0'>---Select---</option>");
							$.each(response, function(index, value) {
								$('#questionCode').append(
										'<option value="' + value.questionCode + '">'
												+ value.questionName
												+ '</option>');
							});

						},
						error : function(jqXHR) {
							$(document.body).text('Error: ' + jqXHR.status);
						}
					});
				});

	}
	function changeFunc() {
		var selectBox = document.getElementById("questionCode");
		var selectedValue = selectBox.options[selectBox.selectedIndex].value;
		$(document).ready(
				function() {
					$.ajax({
						type : 'POST',
						url : 'http://localhost:8080/question/options',
						dataType : "json",
						data : {
							qCode : selectedValue
						},
						success : function(response) {

							$("#optionCode").empty();
							$('#optionCode').append(
									"<option value='0'>---Select---</option>");
							$.each(response, function(index, value) {
								$('#optionCode').append(
										'<option value="' + value.optionCode + '">'
												+ value.option + '</option>');
							});

						},
						error : function(jqXHR) {
							$(document.body).text('Error: ' + jqXHR.status);
						}
					});
				});

	}
	function submitAnswer() {

		var frm = document.answerForm;
		if (frm.categoryName.value == "" || frm.categoryName.value == "none") {
            frm.categoryName.focus();
            document.getElementById("show_errorCcode").style.display = 'block';
            document.getElementById("show_errorQcode").style.display = 'none';
            document.getElementById("show_errorOcode").style.display = 'none';
            return false;
        }
        else{
            frm.categoryName.focus();
            document.getElementById("show_errorCcode").style.display = 'none';
        }
        if (frm.questionCode.value == "" || frm.questionCode.value == 0) {
            frm.questionCode.focus();
            document.getElementById("show_errorQcode").style.display = 'block';
            document.getElementById("show_errorCcode").style.display = 'none';
            document.getElementById("show_errorOcode").style.display = 'none';
            return false;
        }else{
            frm.questionCode.focus();
            document.getElementById("show_errorQcode").style.display = 'none';
        }
        if (frm.optionCode.value == "" || frm.optionCode.value == 0) {
            frm.optionCode.focus();
            document.getElementById("show_errorOcode").style.display = 'block';
            document.getElementById("show_errorCcode").style.display = 'none';
            document.getElementById("show_errorQcode").style.display = 'none';
            return false;
        }else{
            frm.optionCode.focus();
            document.getElementById("show_errorOcode").style.display = 'none';
        }

		jQuery(document).ready(function($) {

		//	$("#submit").click(function() {
				var developerData = {};
				developerData["questionName"] = $("#questionCode").val();
				developerData["optionCode"] = $("#optionCode").val();

				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "http://localhost:8080/question/answers",
					data : JSON.stringify(developerData),
					dataType : 'json',
					success : function(data) {
						if (data.status == true) {
							$('#successdiv').show();
							$('#errordiv').hide();
						} else {
							$('#successdiv').hide();
							$('#errordiv').show();

						}
						document.getElementById("questionCode").value = "";
						document.getElementById("optionCode").value = "";

					}
				});
			//});

		});

	}
</script>

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"> <img
					src="/resources/static/images/Coforge_Logo_Background.png"
					style="width: 50px;">
				</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">${user.name}</a></li>
				<li class="active"><a href="../home">Home</a></li>
				<li><a href="../profile">Change Password</a></li>
				<li><a href="../question/">Add Question</a></li>
				<li><a href="/question/category">Add Category</a></li>
				<li><a href="/question/answer">Add Answer</a></li>
			</ul>
			<ul class="nav navbar-right"
				style="margin-left: auto; margin-right: 0px;">
				<li><a class="btn btn-danger navbar-btn"
					href="../logout">Logout</a></li>
			</ul>

		</div>
	</nav>
	<img src="/resources/static/images/Coforge_Logo_Background.png"
		alt="Coforge Ltd"
		style="margin-top: 60px; width: 250px; height: 60px;">
	<div class="container">
		<div class="row">
			<div class="col">
				<br> <br> <br> <br>
				<form name="answerForm" method="post" name="saveAnswer">

					<div id="successdiv" style="display: none; color: #32c532">
						<h3>Answer is successfully submitted.</h3>
					</div>

					<div id="errordiv" style="display: none; color: red">
						<h3>Error in submitting answer.</h3>
					</div>
					<h3>Add Answer</h3>
					<br>
					<div>
						<span id="show_errorCcode" class="text-danger"
							style="display: none;"><strong>Category can not
								be empty!</strong></span> Select a Category:&nbsp; <select name="categoryName"
							id="categoryName" class="form-control"
							onchange="changeCategory();">
							<option value="none">---Select---</option>
							<c:forEach items="${categoryList}" var="category">
								<option value="${category.categoryCode}">${category.categoryName}</option>
							</c:forEach>
						</select>
					</div>
					<br>
					<div>
						<span id="show_errorQcode" class="text-danger"
							style="display: none;"><strong>question can not
								be empty!</strong></span> Select a question:&nbsp; <select name="questionCode"
							id="questionCode" class="form-control" onchange=changeFunc();>

						</select>
					</div>
					<br />
					<div>
						<span id="show_errorOcode" class="text-danger"
							style="display: none;"><strong>Option can not be
								empty!</strong></span> Select a Option:&nbsp; <select name="optionCode"
							id="optionCode" class="form-control">

						</select>
					</div>
					<br />
					<div>
						<input type="button" onclick="submitAnswer()" id="submit"
							value="Save Answer" class="btn btn-info" />
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>