<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>
<title>LOGIN PAGE</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/resources/static/css/style.css">

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"> <img
					src="/resources/static/images/Coforge_Logo_Background.png"
					style="width: 50px;">
				</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="../">Home</a></li>
				<li><a href="#"></a></li>
				<li><a href="#"></a></li>
				<li><a href="#"></a></li>
			</ul>
		</div>
	</nav>

	<div class="container">
		<div class="row">
			<div class="col">
				<form action="register" method="post">
					<h3>Register</h3>
					<input type="text" name="name" placeholder="User ID"
						required="required"> <input type="text" name="mailId"
						placeholder="Email ID" required="required"> <input
						type="password" name="password" placeholder="Password"
						required="required"> <!-- <label for="role">Role: </label> <select
						name="role" id="role" class="form-control">
						<option value="Admin" selected="selected">Admin</option>
						<option value="User">User</option> </select>-->
						<input type="hidden" name="role" value="User">
					 <input type="submit" value="Register" />
				</form>
			</div>
		</div>
	</div>
</body>
</html>