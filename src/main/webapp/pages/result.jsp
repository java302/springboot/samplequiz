<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Result Page</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/resources/static/css/style.css">
<script src="/resources/static/js/home.js"></script>
</head>
<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"> <img
					src="/resources/static/images/Coforge_Logo_Navbar.png"
					style="width: 50px;">
				</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">${user.name}</a></li>
				<li class="active"><a href="../home">Home</a></li>
				<li><a href="#"></a></li>
				<li><a href="#"></a></li>
			</ul>
			<ul class="nav navbar-right"
				style="margin-left: auto; margin-right: 0px;">
				<li><a class="btn1 btn-danger navbar-btn"
					href="../logout">Logout</a></li>
			</ul>
		</div>
	</nav>

	<img src="/resources/static/images/Coforge_Logo_Background.png"
		alt="Coforge Ltd"
		style="margin-top: 60px; width: 250px; height: 60px;">

	<table class="table table-striped table-bordered">
		<thead>
			<tr class="text-dark text-center">
				<th scope="col">ID</th>
				<th scope="col">Name</th>
				<th scope="col">Email</th>
				<th scope="col">Category</th>
				<th scope="col">Marks</th>
				<th scope="col">Date</th>
			</tr>
		</thead>

		<tbody>
			<c:forEach var="result" items="${resultList}">
				<tr class="text-dark">
					<td>${result.id}</td>
					<td>${result.name}</td>
					<td>${result.mailId}</td>
					<td>${result.category}</td>
					<td>${result.marks}</td>
					<td>${result.date}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>