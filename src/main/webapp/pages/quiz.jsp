<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>
<title>Challenge Your Colleagues</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script type="text/javascript">
var min_limit = 10; // Min Limit
$(document).ready(function(){
    $("form").submit(function(){
		 if ($('input:checkbox').filter(':checked').length < min_limit){
		        alert("Please select at least 10 questions to challenge a colleagues.");
		 return false;
		 }
    });	
});
/* var min_limit = 10; // Min Limit
var max_limit = 10; // Max Limit

$(document).ready(function(){
    $("form").submit(function(){
		 if ($('input:checkbox').filter(':checked').length < min_limit){
		        alert("Please select at least 10 questionsx to challenge a colleagues.");
		 return false;
		 }
		 
		 $(".chkBtnList:input:checkbox").each(function (index){
		        this.checked = (".chkBtnList:input:checkbox" < min_limit);
		    }).change(function (){
		        if ($(".chkBtnList:input:checkbox:checked").length > max_limit){
		            alert("max 10 selection allowed.")
		        	this.checked = false;
		       	}
		 });
    });
}); */
/* 
$(document).ready(function (){
    $(".chkBtnList:input:checkbox").each(function (index){
        this.checked = (".chkBtnList:input:checkbox" < min_limit);
    }).change(function (){
        if ($(".chkBtnList:input:checkbox:checked").length > max_limit){
            //alert("max 3 selection allowed.")
        	this.checked = false;
            
        }
    });
}); */
</script>
<style>
#css-serial {
  counter-reset: serial-number;  /* Set the serial number counter to 0 */
}

#css-serial td:first-child:before {
  counter-increment: serial-number;  /* Increment the serial number counter */
  content: counter(serial-number);  /* Display the counter */
}
.table {
    width: 70%;
    max-width: 100%;
    margin-bottom: 30px;
}
</style>
</head>

<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"> <img
					src="/resources/static/images/Coforge_Logo_Navbar.png"
					style="width: 50px;">
				</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">${user.name}</a></li>
				<li class="active"><a href="../home">Home</a></li>
				<%-- <li class="active"><a href="#">Category: ${categoryList.categoryName}</a></li> --%>
			</ul>
			<ul class="nav navbar-right"
				style="margin-left: auto; margin-right: 0px;">
				<li><a class="btn btn-danger navbar-btn"
					href="../logout">Logout</a></li>
			</ul>
		</div>
	</nav>

	<img src="/resources/static/images/Coforge_Logo_Background.png"
		alt="Coforge Ltd"
		style="margin-top: 80px; width: 250px; height: 60px; display: block; margin-left: auto; margin-right: auto;">

	<h2 align="center">
		<font><strong>Create Quiz</strong></font>
	</h2>
	
	<h2 align="center">
		<font><strong>Challenge Your Colleagues</strong></font>
	</h2>


<form action="/quiz/review" method="post">
		<table id="css-serial" align="center" class="table table-striped table-bordered table-sm">
			<tr>

			</tr>
			<tr>
			<thead>
				<th>Q.Num</th>
				<th>Questions</th>
			</thead>
			</tr>
			<c:forEach var="questions" begin="0"
						end="${ questionList.size() - 1 }" items="${questionList}" varStatus="loop">
			<tr>
				<td>&nbsp;</td>
				<td><input type="checkbox" name="qList" class="chkBtnList"
					value="${questions.questionCode}"> ${questions.question}</td>
			</tr>
		
			</c:forEach>
		</table>
			<input type="hidden" name="category" id="category" value="${selectdCategory}">
		
		<p></p>
		<center>
		<% String[] qList = request.getParameterValues("qList"); %>
			<button class="btn btn-primary" type="submit" id="submit">Review</button>
		</center>
	</form>

</body>
</html>