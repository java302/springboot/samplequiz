<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>
<title>Coforge LFG Learning Pad</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/resources/static/css/style.css">
<script src="/resources/static/js/home.js"></script>
</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"> <img
					src="/resources/static/images/Coforge_Logo_Navbar.png"
					style="width: 50px;">
				</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">${user.name}</a></li>
				<li class="active"><a href="../home">Home</a></li>
				<li><a href="../profile">Change Password</a></li>
				<li><a href="../question/">Add Question</a></li>
				<li><a href="../question/category">Add Category</a></li>
				<li><a href="../result">Display Result</a></li>
				<li><a href="/quiz/submitQuizCategory">Challenge Your
						Colleagues</a></li>
			</ul>
			<ul class="nav navbar-right"
				style="margin-left: auto; margin-right: 0px;">
				<li><a class="btn1 btn-danger navbar-btn"
					href="../logout">Logout</a></li>
			</ul>
		</div>
	</nav>

	<img src="/resources/static/images/Coforge_Logo_Background.png"
		alt="Coforge Ltd"
		style="margin-top: 60px; width: 250px; height: 60px;">


	<div class="container">
		<div class="row">
			<div class="col">

				<h3>Categories</h3>
				<form action="/quiz/getQuestions?categoryCode=${categoryCode}"
					method="get">

					<%
						String challengeCategoryCode = (String)request.getAttribute("categoryCode");
						if (null == challengeCategoryCode || challengeCategoryCode.trim().equals("")) {
					%>

					<select name="categoryCode" id="categoryCode" class="form-control"
						style="width: 350px;" onchange=getCategoryCode();>
						<c:forEach items="${categoryList}" var="category">
							<option value="${category.categoryCode}">${category.categoryName}</option>

						</c:forEach>
					</select>

					<%
						} else {
					%>

					<select name="categoryCode" id="categoryCode" class="form-control"
						style="width: 350px;" onchange=getCategoryCode(); disabled>
						<c:forEach items="${categoryList}" var="category">
							<c:if test="${category.categoryCode==categoryCode}">
								<option value="${category.categoryCode}" selected>${category.categoryName}</option>
							</c:if>
							<c:if test="${category.categoryCode!=categoryCode}">
								<option value="${category.categoryCode}">${category.categoryName}</option>
							</c:if>
						</c:forEach>
					</select> <input type="hidden" name="categoryCode"
						value="<%=request.getAttribute("categoryCode")%>">
					<%
						}
						String challengeId = (String)request.getAttribute("challengeId");
						if(challengeId ==null){
							challengeId = "0";
						}
					%>

					<input type="submit" value="Start Quiz" /> <input type="hidden"
						name="page" value="0"> <input type="hidden"
						name="challengeId"
						value="<%=challengeId%>">

				</form>
			</div>
		</div>
	</div>

</body>
</html>