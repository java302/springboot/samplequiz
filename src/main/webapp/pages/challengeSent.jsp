<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>
<title>Review</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<style>
.css-serial {
  counter-reset: serial-number;  /* Set the serial number counter to 0 */
}

.css-serial td:first-child:before {
  counter-increment: serial-number;  /* Increment the serial number counter */
  content: counter(serial-number);  /* Display the counter */
}
</style>

</head>

<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.coforge.entities.QuizChallengeDetails"%>
<%@page import="java.util.List"%>

<%
List<QuizChallengeDetails> updQuizChallengeContestantDet = (List<QuizChallengeDetails>) request.getAttribute("updQuizChallengeContestantDet");
%>

<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"> <img
					src="/resources/static/images/Coforge_Logo_Navbar.png"
					style="width: 50px;">
				</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">${user.name}</a></li>
				<li class="active"><a href="../home">Home</a></li>
				<%-- <li class="active"><a href="#">Category: ${categoryList.categoryName}</a></li> --%>
			</ul>
			<ul class="nav navbar-right"
				style="margin-left: auto; margin-right: 0px;">
				<li><a class="btn btn-danger navbar-btn"
					href="../logout">Logout</a></li>
			</ul>
		</div>
	</nav>

	<img src="/resources/static/images/Coforge_Logo_Background.png"
		alt="Coforge Ltd"
		style="margin-top: 80px; width: 250px; height: 60px; display: block; margin-left: auto; margin-right: auto;">


	<h4 align="center">
		<font><strong>Challenge link sent to the following user(s):</strong></font>
	</h4>
	<table align="center" cellpadding="5" cellspacing="5" border="1">
	    <tr>
			<td><font><strong>Email ID</strong></font></td>
			<td><font><strong>Challenge send status</strong></font></td>
		</tr>
		<%
		QuizChallengeDetails challengeContestantDet = null;
		for (int i = 0; i < updQuizChallengeContestantDet.size(); i++) {
			challengeContestantDet = updQuizChallengeContestantDet.get(i);
		%>
		<tr>
			<td><font><%=challengeContestantDet.getEmailId()%></font></td>
			<td><font><%=challengeContestantDet.getEmailSendStatus()%></font></td>
		</tr>
		<%
		}
		%>
	</table>
</body>
</html>