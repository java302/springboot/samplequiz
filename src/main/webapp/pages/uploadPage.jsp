<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>File Upload Page</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/resources/static/css/upload.css">
<style type="text/css">
.form-control {
	height: 42px;
}

input#file-upload-button {
	margin: 0px 0px 0px 0px;
}
</style>
<script type="text/javascript">
	function ValidateExtension() {
		var allowedFiles = [ ".xls", ".xlsx" ];
		var fileUpload = document.getElementById("fileUpload");
		var lblError = document.getElementById("lblError");
		var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+("
				+ allowedFiles.join('|') + ")$");

		if (!regex.test(fileUpload.value.toLowerCase())) {
			lblError.innerHTML = "Please upload files having extensions: <b>"
					+ allowedFiles.join(', ') + "</b> only.";
			return false;
		}

		lblError.innerHTML = "";
		return true;
	}
	function changeFile() {
		lblError.innerHTML = "";
		document.getElementById("lblMsg").style.display = "none";
		document.getElementById("lblSuccess").style.display = "none";
		return false;
	}
</script>
</head>

<body style="width: 95%;">
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#"> <img
                    src="/resources/static/images/Coforge_Logo_Background.png"
                    style="width: 50px;">
                </a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">${user.name}</a></li>
                <li class="active"><a href="../home">Home</a></li>
                <li><a href="../profile">Change Password</a></li>
                <li><a href="../question/">Add Question</a></li>
                <li><a href="/question/category">Add Category</a></li>
                <!-- <li><a href="/question/answer">Add Answer</a></li> -->
                <li><a href="../result">Display Result</a></li>
            </ul>
            <ul class="nav navbar-right"
                style="margin-left: auto; margin-right: 0px;">
                <li><a class="btn btn-danger navbar-btn"
                    href="../logout">Logout</a></li>
            </ul>

 

        </div>
    </nav>
	<div class="container">
		<div class="row">
			<div class="col">
				<h3>Upload File</h3>
				<form action="uploadFile" method="POST"
					enctype="multipart/form-data">

					<div>
						<span id="lblError" style="color: red;"></span>
					</div>
					<input type="file" id="fileUpload" onclick="changeFile()"
						name="file" value="Browse File"
						class="form-control form-control-lg" /> <input type="submit"
						id="btnUpload" value="Upload" onclick="return ValidateExtension()" />

					<h5 style="color: green" id="lblMsg">${message}</h5>

					<c:if test="${not empty fname}">
						<span id="lblSuccess">Do you want to save excel data into
							database ? <a href="saveData?fname=${fname}"><br>
							<b>Yes</b></a> &nbsp &nbsp <a href="../question/"><b>No</b></a>
						</span>
					</c:if>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
