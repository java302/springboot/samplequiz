<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>
<title>Register Form</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/resources/static/css/register.css">

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"> <img
					src="/resources/static/images/Coforge_Logo_Background.png"
					style="width: 50px;">
				</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="../">Home</a></li>
				<li><a href="#"></a></li>
				<li><a href="#"></a></li>
				<li><a href="#"></a></li>
			</ul>
		</div>
	</nav>
	<img src="/resources/static/images/Coforge_Logo_Background.png"
		alt="Coforge Ltd"
		style="margin-top: 60px; width: 250px; height: 60px;">
	<div class="container">
		<div class="row">
			<div class="col">
				<form:form action="register" method="post" modelAttribute="user">
					<h3>Register</h3>
					<form:input type="text" path="name" placeholder="User ID"
						required="required" />
					<form:errors path="name" style="color:red;" />
					<form:input type="text" path="mailId" placeholder="Email ID"
						required="required" />
					<form:errors path="mailId"  style="color:red;" />
					<form:input type="password" path="password" placeholder="Password"
						required="required" />
					<form:errors path="password" style="color:red;"/>
					<br />
					<!-- <label for="role">Role: </label>
					<select name="role" id="role" class="form-control">
						<option value="Admin" selected="selected">Admin</option>
						<option value="User">User</option>
					</select> -->
					<input type="hidden" name="role" value="User">
					<input type="submit" value="Register" />
				</form:form>
			</div>
		</div>
	</div>
</body>
</html>