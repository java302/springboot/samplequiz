<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>
<title>Category PAGE</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="/resources/static/css/validation.js"></script>
<link rel="stylesheet" href="/resources/static/css/category.css">

<script type="text/javascript">
	function submitCategory() {

		var frm = document.categoryForm;
		var frm = document.categoryForm;
		if (frm.categoryCode.value == "") {
			frm.categoryCode.focus();
			document.getElementById("show_errorCode").style.display = 'block';
			document.getElementById("show_errorName").style.display = 'none';

			return false;
		} else {
			frm.categoryCode.focus();
			document.getElementById("show_errorCode").style.display = 'none';
		}
		if (frm.categoryName.value == "") {
			frm.categoryName.focus();
			document.getElementById("show_errorName").style.display = 'block';
			document.getElementById("show_errorCode").style.display = 'none';
			return false;
		} else {
			frm.categoryName.focus();
			document.getElementById("show_errorName").style.display = 'none';
		}
		jQuery(document).ready(function($) {

			//	$("#submit").click(function() {
			var developerData = {};
			developerData["categoryCode"] = $("#categoryCode").val();
			developerData["categoryName"] = $("#categoryName").val();

			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "http://localhost:8080/question/categories",
				data : JSON.stringify(developerData),
				dataType : 'json',
				success : function(data) {
					if (data.status == true) {
						$('#successdiv').show();
						$('#errordiv').hide();
						document.getElementById("categoryCode").value = "";
						document.getElementById("categoryName").value = "";
					} else {
						$('#successdiv').hide();
						$('#errordiv').show();

					}

				}
			});
			//});

		});
	}
</script>

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"> <img
					src="/resources/static/images/Coforge_Logo_Background.png"
					style="width: 50px;">
				</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">${user.name}</a></li>
				<li class="active"><a href="../home">Home</a></li>
				<li><a href="../profile">Change Password</a></li>
				<li><a href="../question/">Add Question</a></li>
				<li><a href="/question/category">Add Category</a></li>
				<!-- <li><a href="/question/answer">Add Answer</a></li> -->
				<li><a href="../result">Display Result</a></li>
			</ul>
			<ul class="nav navbar-right"
				style="margin-left: auto; margin-right: 0px;">
				<li><a class="btn btn-danger navbar-btn"
					href="../logout">Logout</a></li>
			</ul>

		</div>
	</nav>
	<img src="/resources/static/images/Coforge_Logo_Background.png"
		alt="Coforge Ltd"
		style="margin-top: 60px; width: 250px; height: 60px;">
	<div class="container">
		<div class="row">
			<div class="col">
				<form action="" method="post" class="form-group" name="categoryForm">


					<h3>Add Category</h3>
					<div id="successdiv" style="display: none; color: #32c532">
						<h5>Category is successfully submitted.</h5>
					</div>

					<div id="errordiv" style="display: none; color: red">
						<h5>Category already exist.</h5>
					</div>
					<div>
						<span id="show_errorCode" class="text-danger"
							style="display: none;"><strong>Category Code can
								not be empty!</strong></span> <input type="text" name="categoryCode"
							id="categoryCode" placeholder="Category Code" required="required"
							class="form-control">
					</div>

					<div>
						<span id="show_errorName" class="text-danger"
							style="display: none;"><strong>Category Name can
								not be empty!</strong></span><input type="text" name="categoryName"
							id="categoryName" placeholder="Category Name" required="required"
							class="form-control">
					</div>
					<div class="text-center">
						<input type="button" onclick="submitCategory()" id="submit"
							value="Save Category" class="btn btn-info" />
					</div>
				</form>

			</div>
		</div>
	</div>
</body>
</html>