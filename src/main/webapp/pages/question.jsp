<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>
<title>QUESTION PAGE</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">

	function submitFunc() {

		var frm = document.questionForm;
		
		 if (frm.categoryCode.value == "" || frm.categoryCode.value == "none")
	        {
	            document.getElementById("show_errorCcode").style.display = 'block';
	            document.getElementById("show_errorop1").style.display = 'none';
	            document.getElementById("show_errorop2").style.display = 'none';
	            document.getElementById("show_errorQname").style.display = 'none';
	            document.getElementById("show_erroranswer").style.display = 'none';
				document.getElementById("show_erroranswervalid").style.display = 'none';
				document.getElementById("show_errorDiffrentOp").style.display = 'none';
	            return false;
	        }else{
	             document.getElementById("show_errorCcode").style.display = 'none';
	        }
	        if (frm.questionName.value == "")
	        {
	            document.getElementById("show_errorQname").style.display = 'block';
	            document.getElementById("show_errorop1").style.display = 'none';
	            document.getElementById("show_errorop2").style.display = 'none';
	            document.getElementById("show_erroranswer").style.display = 'none';
				document.getElementById("show_erroranswervalid").style.display = 'none';
				document.getElementById("show_errorDiffrentOp").style.display = 'none';
	            return false;
	        }else{
	            document.getElementById("show_errorQname").style.display = 'none';
	        }
	        if (frm.option1.value == "")
	        {
	            document.getElementById("show_errorop1").style.display = 'block';
	            document.getElementById("show_errorop2").style.display = 'none';
	            document.getElementById("show_erroranswer").style.display = 'none';
				document.getElementById("show_erroranswervalid").style.display = 'none';
				document.getElementById("show_errorDiffrentOp").style.display = 'none';
	            return false;
	        }else{
	        	 document.getElementById("op1").disabled = false;
	            document.getElementById("show_errorop1").style.display = 'none';
	        }
	        if (frm.option2.value == "")
	        {
	        	
	            document.getElementById("show_errorop2").style.display = 'block';
	            document.getElementById("show_errorop1").style.display = 'none';
	            document.getElementById("show_erroranswer").style.display = 'none';
				document.getElementById("show_erroranswervalid").style.display = 'none';
				document.getElementById("show_errorDiffrentOp").style.display = 'none';
	            return false;
	        }else{
	        	document.getElementById("op2").disabled = false;
	            document.getElementById("show_errorop2").style.display = 'none';
	        }
	     
		var allTextBoxes = [];

		$('input[type=text]').each(function() {
			if ($(this).val() != "") {
				allTextBoxes.push($(this).val())
				
			}

		});

		var sorted_arr = allTextBoxes.sort();
		for (var i = 0; i < allTextBoxes.length - 1; i++) {
			if (sorted_arr[i + 1] == sorted_arr[i]) {
				document.getElementById("show_errorDiffrentOp").style.display = 'block';
				return false;
			}else{
				document.getElementById("show_errorDiffrentOp").style.display = 'none';
			}
		}
		
		  if ($('input[name="answerCode"]:checked').length == 0) {
			
			 document.getElementById("show_erroranswer").style.display = 'block';
			 document.getElementById("show_erroranswervalid").style.display = 'none';
	         return false; 
	        } else{
	        	var answerCode = document.querySelector('input[name = "answerCode"]:checked').value;
	        
	        	if(answerCode == "option3"){
	        		if(frm.option3.value == ""){
	        			document.getElementById("show_erroranswer").style.display = 'none';
	        			document.getElementById("show_erroranswervalid").style.display = 'block';
	        			return false;
	        		}
	        	}
	        	if(answerCode == "option4"){
	        		if(frm.option4.value == ""){
	        			document.getElementById("show_erroranswervalid").style.display = 'block';
	        			return false;
	        		}
	        	}
	        	document.getElementById("show_erroranswer").style.display = 'none';
	        } 
		jQuery(document).ready(function($) {
		var ele = document.getElementsByName('answerCode');
		
				var developerData = {};
				developerData["categoryCode"] = $("#categoryCode").val();
				developerData["questionName"] = $("#questionName").val();
				developerData["option1"] = $("#option1").val();
				developerData["option2"] = $("#option2").val();
				developerData["option3"] = $("#option3").val();
				developerData["option4"] = $("#option4").val();
				
				for(i = 0; i < ele.length; i++) { 
		            if(ele[i].checked){ 
		            var answerCode = ele[i].value;
		            //alert(answerCode);
		            developerData["correctOption"] =  $("#"+answerCode).val();
		            }
		        } 
				
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "http://localhost:8080/question/questions",
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						if(data.status == true){
							$('#successdiv').show();
							$('#errordiv').hide();
							document.getElementById("categoryCode").value = "";
							document.getElementById("questionName").value = "";
							document.getElementById("option1").value = "";
							document.getElementById("option2").value = "";
							document.getElementById("option3").value = "";
							document.getElementById("option4").value = "";
							document.getElementByName("answerCode").value = "";
						}else{
							$('#successdiv').hide();
							$('#errordiv').show();
							
						}
						
					}
				});

		}); 


	}


</script>

</head>

<body>


	<div class="container">
		<div class="row">
			<div class="col">
				<div>
					<div id="successdiv" style="display: none; color: #32c532">
						<h5>Question is successfully submitted.</h5>
					</div>
					<div id="errordiv" style="display: none; color: red">
						<h5>Question already exist. Please enter a new question.</h5>
					</div>
				</div>
				<form:form name="questionForm" method="post"
					modelAttribute="question">

					<span id="show_erroranswer" class="text-danger"
						style="display: none;"> <strong>Please select a
							radio button to choose answer!</strong></span>
					<span id="show_erroranswervalid" class="text-danger"
						style="display: none;"> <strong>Selected answer
							option has no value! Please choose the correct one.</strong></span>
					<span id="show_errorDiffrentOp" class="text-danger"
						style="display: none; text-align: center;"> <strong>Please
							enter different value in each option!</strong></span>
					<br>
					<span id="show_errorCcode" class="text-danger"
						style="display: none;"><strong>Category can not be
							empty!</strong></span>
					<span id="show_errorQname" class="text-danger"
						style="display: none;"><strong>Question can not be
							empty!</strong></span>
					<span id="show_errorop1" class="text-danger" style="display: none;"><strong>Option
							1 can not be empty!</strong></span>
					<span id="show_errorop2" class="text-danger" style="display: none;"><strong>Option
							2 can not be empty!</strong></span>
					<h3>Question Details</h3>
					<table align="left">
						<tr>
							<td colspan="2"><select name="categoryCode"
								style="width: 145%;" id="categoryCode"
								class="form-control form-control-lg" onchange=getCategoryCode();>
									<option value="none">---Select Category---</option>
									<c:forEach items="${categoryList}" var="category">
										<option value="${category.categoryCode}">${category.categoryName}</option>

									</c:forEach>
							</select></td>
						</tr>
						<tr>
							<td colspan="2"><form:textarea type="text"
									style="width:145%;" path="questionName" name="questionName"
									id="questionName" placeholder="Question"
									class="form-control form-control-lg"></form:textarea></td>
						</tr>
						<tr>
							<td width="10%"><form:radiobutton path="answerCode"
									name="answerCode" id="op1" value="option1" disabled="false" /></td>
							<td width="100%"><form:input type="text" path="option1"
									id="option1" name="option1" placeholder="option1"
									class="form-control form-control-lg" /></td>
						</tr>
						<tr>
							<td width="10%"><form:radiobutton path="answerCode"
									name="answerCode" id="op2" value="option2" disabled="false" /></td>
							<td width="100%"><form:input type="text" path="option2"
									id="option2" name="option2" placeholder="option2"
									class="form-control form-control-lg" /></td>
						</tr>
						<tr>

							<td width="10%"><form:radiobutton path="answerCode"
									name="answerCode" id="op3" value="option3" disabled="false" /></td>
							<td width="100%"><form:input type="text" path="option3"
									id="option3" name="option3" placeholder="option3"
									class="form-control form-control-lg" /></td>
						</tr>
						<tr>
							<td width="10%"><form:radiobutton path="answerCode"
									name="answerCode" id="op4" value="option4" disabled="false" /></td>
							<td width="100%"><form:input type="text" path="option4"
									id="option4" name="option4" placeholder="option4"
									class="form-control form-control-lg" /></td>
						</tr>
					</table>
					<div class="text-center">
						<br> <input onclick="submitFunc()" type="button"
							value="Save Question" />
					</div>
				</form:form>


			</div>
		</div>
	</div>
</body>
</html>