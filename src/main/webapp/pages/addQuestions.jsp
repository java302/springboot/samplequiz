<!DOCTYPE html>
<html>

<head>
<title>Coforge LFG Learning Pad</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- <link rel="stylesheet" href="/resources/static/css/style.css"> -->
<script src="/resources/static/css/validation.js"></script>

</head>

<body style="width: 95%;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"> <img
					src="/resources/static/images/Coforge_Logo_Background.png"
					style="width: 50px;">
				</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">${user.name}</a></li>
				<li class="active"><a href="../home">Home</a></li>
				<li><a href="../profile">Change Password</a></li>
				<li><a href="../question/">Add Question</a></li>
				<li><a href="/question/category">Add Category</a></li>
				<!-- <li><a href="/question/answer">Add Answer</a></li> -->
				<li><a href="../result">Display Result</a></li>
			</ul>
			<ul class="nav navbar-right"
				style="margin-left: auto; margin-right: 0px;">
				<li><a class="btn btn-danger navbar-btn"
					href="../logout">Logout</a></li>
			</ul>

		</div>
	</nav>
	<!-- <img src="/resources/static/images/Coforge_Logo_Background.png"
		alt="Coforge Ltd"
		style="margin-top: 80px; width: 250px; height: 60px;"> -->

	<div class="row">
		<div class="col-lg-6">
			<jsp:include page="question.jsp" />
		</div>

		<div class="col-lg-6">
			<jsp:include page="uploadPage.jsp" />
		</div>

	</div>
</body>

</html>