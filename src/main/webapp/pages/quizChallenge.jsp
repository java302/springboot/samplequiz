<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>
<title>Review</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<style>
.css-serial {
  counter-reset: serial-number;  /* Set the serial number counter to 0 */
}

.css-serial td:first-child:before {
  counter-increment: serial-number;  /* Increment the serial number counter */
  content: counter(serial-number);  /* Display the counter */
}
</style>

<script type="text/javascript">
var textIncr = 1;
var checkboxIncr = 1;

function addRow(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount);
	var colCount = table.rows[0].cells.length;
    
	
	for(var i=0; i<colCount; i++) {
		var newcell	= row.insertCell(i);
		newcell.innerHTML = table.rows[0].cells[i].innerHTML;
		//alert(newcell.childNodes);
		switch(newcell.childNodes[0].type) {
			case "text":
				    newcell.childNodes[0].name = "text"+textIncr;
					newcell.childNodes[0].value = "";
					textIncr++;
					break;
			case "checkbox":
				    newcell.childNodes[0].name = "checkbox"+checkboxIncr;
					newcell.childNodes[0].checked = false;
					checkboxIncr++;
					break;
		}
	}
}

function deleteRow(tableID) {
	try {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;

	for(var i=0; i<rowCount; i++) {
		var row = table.rows[i];
		var chkbox = row.cells[0].childNodes[0];
		if(null != chkbox && true == chkbox.checked) {
			if(rowCount <= 1) {
				alert("Cannot delete all the rows.");
				break;
			}
			table.deleteRow(i);
			rowCount--;
			i--;
		}
	}
	}catch(e) {
		alert(e);
	}
}
</script>
</head>
<body>
<%
String category = request.getParameter("category");
String challengeId = request.getParameter("challenge_id");

%>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"> <img
					src="/resources/static/images/Coforge_Logo_Navbar.png"
					style="width: 50px;">
				</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">${user.name}</a></li>
				<li class="active"><a href="../home">Home</a></li>
				<%-- <li class="active"><a href="#">Category: ${categoryList.categoryName}</a></li> --%>
			</ul>
			<ul class="nav navbar-right"
				style="margin-left: auto; margin-right: 0px;">
				<li><a class="btn btn-danger navbar-btn"
					href="../logout">Logout</a></li>
			</ul>
		</div>
	</nav>

	<img src="/resources/static/images/Coforge_Logo_Background.png"
		alt="Coforge Ltd"
		style="margin-top: 80px; width: 250px; height: 60px; display: block; margin-left: auto; margin-right: auto;">

	<h2 align="center">
		<font><strong>Challenge recipient details</strong></font>
	</h2>
	<p></p>
	<div class="container">
		<div class="row">
			<div class="col">
				<form method="post" action="/quiz/challengeSent"
					name="reviewForm">
					<center>
						<!-- <INPUT class="btn btn-primary" type="button" value="Add Row" onclick="addRow('dataTable')" />
						<INPUT class="btn btn-primary" type="button" value="Delete Row" onclick="deleteRow('dataTable')" /> -->
						<p></p>
						<TABLE id="dataTable" border="0">
							<TR>
								<!-- <TD><INPUT type="checkbox" name="chk" /></TD> -->
								<TD>Email Id(s) :&nbsp;</TD>
								<TD><INPUT type="text" name="emailIds" placeholder="email id"/></TD>
								<TD><h6 align="center"><font><strong>&nbsp;eg. abc@coforgetech.com,def@coforgetech.com</strong></font></h6> </TD>
							</TR>
						</TABLE>
						<p></p>
						<input class="btn btn-primary" type=button value="Back"
							onCLick="history.back()"> <input class="btn btn-primary"
							type=submit value="Submit Challenge">
					</center>
					<input type="hidden" name="category" id="category" value="<%=category%>">
					<input type="hidden" name="challenge_id" id="challenge_id" value="${challenge_id}">
					<!-- hidden2-->
				</form>
			</div>
		</div>
	</div>
</body>
</html>